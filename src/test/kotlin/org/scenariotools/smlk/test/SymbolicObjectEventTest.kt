package org.scenariotools.smlk.test

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.scenariotools.smlk.*
import java.lang.IllegalArgumentException

class MyClass(val name : String){
    override fun toString(): String {
        return name
    }

    fun myFun(x : String, y : String) = event(x, y){}

}

object myObject{
    override fun toString(): String {
        return "myObject"
    }

    fun myFun(x : String, y : String) = event(x, y){}

}

class SymbolicObjectEventTest {

    val myA = MyClass("myA")
    val myB = MyClass("myB")



    @Test
    fun testReceivesUtilFunctions(){
        val symbolicObjectEvent = myB receives MyClass::myFun
        val objectEvent = myB.myFun("x", "y")

        Assertions.assertEquals(2, objectEvent.parameters.size)
        Assertions.assertTrue(symbolicObjectEvent.contains(objectEvent))
    }

    @Test
    fun testReceivesUtilFunctionsWithParam(){
        val symbolicObjectEvent = myB receives MyClass::myFun param listOf(ANY, "y")
        val objectEvent = myB.myFun("x", "y")

        Assertions.assertTrue(symbolicObjectEvent.contains(objectEvent))
    }

    @Test
    fun testReceivesUtilFunctionsWithParam2(){
        val symbolicObjectEvent = (myB receives MyClass::myFun).param(ANY, "y")
        val objectEvent = myB.myFun("x", "y")

        Assertions.assertTrue(symbolicObjectEvent.contains(objectEvent))
    }

    @Test
    fun testReceivesUtilFunctionsWithParam3(){
        val symbolicObjectEvent = myB receives MyClass::myFun param ("y" to "Y")
        val objectEvent = myB.myFun("x", "Y")

        Assertions.assertTrue(symbolicObjectEvent.contains(objectEvent))
    }


    @Test
    fun testSendReceivesUtilFunctions(){
        val symbolicObjectEvent = myA sends myB receives MyClass::myFun
        val objectEvent = myA sends myB.myFun("x", "y")

        Assertions.assertTrue(symbolicObjectEvent.contains(objectEvent))
    }

    @Test
    fun testSendReceivesUtilFunctionsWithParam(){
        val symbolicObjectEvent = myA sends myB receives MyClass::myFun param listOf(ANY, ANY)
        val symbolicObjectEvent2 = myA sends myB receives MyClass::myFun param listOf(ANY)
        val objectEvent = myA sends myB.myFun("x", "y")
        val objectEvent2 = myB.myFun("x", "y")

        Assertions.assertTrue(symbolicObjectEvent.contains(objectEvent))
        Assertions.assertTrue(symbolicObjectEvent2.contains(objectEvent))
        Assertions.assertFalse(symbolicObjectEvent.contains(objectEvent2))
    }

    @Test
    fun testObjectSendReceivesUtilFunctions(){
        val objectEvent = myA sends myObject.myFun("x", "y")

        val symbolicObjectEvent = myA sends myObject receives myObject::myFun param listOf(ANY, ANY)

        Assertions.assertTrue(symbolicObjectEvent.contains(objectEvent))
    }

    @Test
    fun testSendReceivesUtilFunctionSymbolicANYSender(){
        val objectEvent = myA sends myObject.myFun("x", "y")
        val symbolicObjectEvent = ANY sends myObject receives myObject::myFun param listOf(ANY, ANY)
        Assertions.assertTrue(symbolicObjectEvent.contains(objectEvent))
    }

    @Test
    fun testSendReceivesUtilFunctionSymbolicNULLSender(){
        val objectEvent = myA sends myObject.myFun("x", "y")
        val symbolicObjectEvent = myObject receives myObject::myFun param listOf(ANY, ANY)
        Assertions.assertTrue(symbolicObjectEvent.contains(objectEvent))
    }

}
