package org.scenariotools.smlk.examples.highvoltagecoupling

import org.scenariotools.smlk.event

//data class Environment(val name : String)
//data class TableSensor(val name : String)
//data class ArmA(val name : String)
//data class ArmB(val name : String)

abstract class NamedElement(val name : String){
    override fun toString(): String {
        return name
    }
}

object Socket : NamedElement("Socket") {
    fun SETLocked(value : Boolean) = event(value){locked = value}
    var locked = false
}

object Relays : NamedElement("Relays") {
    fun SETClosed(value : Boolean) = event(value){closed = value}
    var closed = false
}


object Controller : NamedElement("Controller") {

    fun plugIn() = event(){}
    fun unPlug() = event(){}
    fun startPressed() = event(){}
    fun stopPressed()= event(){}

    fun SETPluggedIn(value : Boolean) = event(value){pluggedIn = value}
    var pluggedIn = false
    var contactClosed = false
    var socketLocked = false
}
