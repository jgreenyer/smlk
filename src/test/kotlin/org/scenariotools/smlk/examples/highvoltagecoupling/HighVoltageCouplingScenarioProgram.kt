package org.scenariotools.smlk.examples.highvoltagecoupling

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.scenariotools.smlk.*


fun highVolageCouplingScenarioProgram() : ScenarioProgram{

    val highVolageCouplingScenarioProgram = ScenarioProgram("HighVoltageCoupling")
    highVolageCouplingScenarioProgram.addEnvironmentMessageType(Controller::startPressed, Controller::stopPressed, Controller::plugIn, Controller::unPlug)

    /*
     * "WhenPluggedThenLock"
     * When the plug is plugged into the socket, then the socket must lock the plug and the controller remembers the plugged-In state
     */
    highVolageCouplingScenarioProgram.activeGuaranteeScenarios.add(
        scenario(
            Controller.plugIn()
        ){
            request(Controller.SETPluggedIn(true))
            request(Socket.SETLocked(true))
        })

    /*
     * "WhenStartPressedThenCloseContact"
     * If the start-button is pressed and a plug is plugged in and the sockets are closed
     * then the relays have to close.
     */
    highVolageCouplingScenarioProgram.activeGuaranteeScenarios.add(scenario(Controller.startPressed()){
        if (Controller.pluggedIn && Socket.locked)
            request(Relays.SETClosed(true))
    })

    /*
     * "WhenRelayClosedUnplugForbidden"
     * When the relays are closed, the plug must not be unplugged.
     */
    highVolageCouplingScenarioProgram.activeGuaranteeScenarios.add(scenario(Controller.unPlug()){
        if (Relays.closed) violation("When the relays are closed, unPlug is forbidden.")
    })


    /*
     * "WhenNotPluggedAndLockedThenCloseRelayForbidden"
     * If the plug is not plugged or not locked then the relays must not make contact.
     */
    highVolageCouplingScenarioProgram.activeGuaranteeScenarios.add(scenario(Relays.SETClosed(true)){
        if (!(Controller.pluggedIn && Socket.locked)) violation("If the plug is not plugged or the socket is not locked then the relays must not make contact.")
    })

    /*
     * "NoUnplugWhenLocked"
     * When locked, unPlug cannot happen
     */
    highVolageCouplingScenarioProgram.activeAssumptionScenarios.add(scenario(Controller.unPlug()){
        if (Socket.locked) violation("The plug cannot be unplugged when the socket is locked.")
    })

    /*
     * "WhenStartPressedThenCloseContact"
     * If the start-button is pressed and a plug is plugged in and the sockets are closed
     * then the relays have to close.
     */
    highVolageCouplingScenarioProgram.activeGuaranteeScenarios.add(scenario(Controller.startPressed()){
        if (Controller.pluggedIn && Socket.locked)
            request(Relays.SETClosed(true))
    })

    /*
     * "WhenStartPressedThenCloseContact"
     * If the stop-button is pressed then first the relays and then the socket is opened.
     */
    highVolageCouplingScenarioProgram.activeGuaranteeScenarios.add(scenario(Controller.stopPressed()){
        request(Relays.SETClosed(false))
        request(Socket.SETLocked(false))
    })

    /*
     * "Unplug"
     * If the stop-button is pressed then first the relays and then the socket is opened.
     */
    highVolageCouplingScenarioProgram.activeGuaranteeScenarios.add(scenario(Controller.unPlug()){
        request(Controller.SETPluggedIn(false))
    })


    return highVolageCouplingScenarioProgram
}

class HVCTest {

    val hvcScenarioProgram = highVolageCouplingScenarioProgram()


    @Test
    fun `unPlug after plugIn and startPressed`() = runTest(
        hvcScenarioProgram,
        expectAssumptionViolation = true,
        expectGuaranteeViolation = true
    ) {
        request(Controller.plugIn())
        request(Controller.startPressed())
        // will lead to a guarantee violation, but also an assumption violation.
        request(Controller.unPlug())
    }

    @Test
    fun `plugIn, startPressed, stopPressed, unPlug leads to not plugged in and not locked`() =
        runTest(hvcScenarioProgram) {
            request(Controller.plugIn())
            request(Controller.startPressed())
            waitFor(Relays.SETClosed(true))
            request(Controller.stopPressed())
            request(Controller.unPlug())
            waitFor(Relays.SETClosed(false))
//            waitFor(Relays.SETClosed(false))
        }

}


