package org.scenariotools.smlk.examples.hotcold

import org.junit.jupiter.api.Test
import org.scenariotools.smlk.*


class Tap {

    fun openTap() = event(){}
    fun addHot() = event(){}
    fun addCold() = event(){}

    override fun toString() = "Tap"
}

val tap = Tap()

fun createScenarioProgram () : ScenarioProgram {

    val scenarioProgram = ScenarioProgram()

    scenarioProgram.addEnvironmentMessageType(Tap::openTap)

    val scenarios = listOf(
        scenario(tap.openTap()){
            repeat(3) {
                request(tap.addHot())
            }
        },
        scenario(tap.openTap()){
            repeat(3) {
                request(tap.addCold())
            }
        },
        scenario(){
            while(true){
                waitFor(tap.addHot(), forbidden = tap.addCold())
                waitFor(tap.addCold(), forbidden = tap.addHot())
            }
        }
    )

    scenarioProgram.activeGuaranteeScenarios.addAll(scenarios)

    return scenarioProgram
}


class HotColdProgramTest{

    val scenarioProgram = createScenarioProgram()

    @Test
    fun testHotCold() = runTest(scenarioProgram, timeoutMillis=2000){
        request(tap.openTap())
        repeat(3){
            mustOccur(tap.addHot())
            mustOccur(tap.addCold())
        }
    }


}