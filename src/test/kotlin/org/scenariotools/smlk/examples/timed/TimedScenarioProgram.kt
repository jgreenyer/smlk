package org.scenariotools.smlk.examples.timed

import org.junit.jupiter.api.Test
import org.scenariotools.smlk.ScenarioProgram
import org.scenariotools.smlk.runTest
import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.symbolicEvent
import java.util.*

fun createTimedScenarioProgram () : ScenarioProgram{

    val scenarioProgram = ScenarioProgram()

    scenarioProgram.addEnvironmentMessageType(System::tick)

    scenarioProgram.activeGuaranteeScenarios.addAll(
        listOf(
            scenario(){
                var i = 0
                while (true){
                    val currentTime = Date().time
                    repeat(10) {
                        waitFor(System::tick.symbolicEvent())
                    }
                    request(System.doA(i++))
                    println ("Scenario waited ${Date().time - currentTime} before doA")
                }
            }
            ,
            scenario(System::doA.symbolicEvent()){
                val currentTime = Date().time
                suspendForMillis(1000)
                request(System.doB(it.parameters[0] as Int))
                println ("Scenario suspended ${Date().time - currentTime} before doB")
            },
            scenario(System::doA.symbolicEvent()){
                val currentTime = Date().time
                suspendForMillis(500)
                request(System.doC(it.parameters[0] as Int))
                println ("Scenario suspended ${Date().time - currentTime} before doC")
            }

        )
    )

    return scenarioProgram
}

class TimedScenarioProgramTest{

    val scenarioProgram = createTimedScenarioProgram()

    @Test
    fun testTimedScenarioProgram() = runTest(scenarioProgram, timeoutMillis=8000){
        repeat(30){
            val currentTime = Date().time
            suspendForMillis(100)
            request(System.tick(it))
            println ("Scenario suspended ${Date().time - currentTime} before doing tick")
        }
        mustOccur(System.doA(2))
        mustOccur(System.doB(2))
    }


}
