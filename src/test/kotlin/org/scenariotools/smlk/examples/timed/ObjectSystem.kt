package org.scenariotools.smlk.examples.timed

import org.scenariotools.smlk.event

abstract class NamedElement(val name : String){
    override fun toString(): String {
        return name
    }
}

object System : NamedElement("System") {

    var a = 0
    var b = 0
    var c = 0

    fun tick(x:Int) = event(x){println("Tick $x")}

    fun doA(x:Int) = event(x){
        println("Doing A $x")
        a = x
    }

    fun doB(x:Int) = event(x){
        println("Doing B $x")
        b = x
    }

    fun doC(x:Int) = event(x){
        println("Doing C $x")
        c = x
    }

}