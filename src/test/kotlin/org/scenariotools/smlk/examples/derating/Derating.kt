package org.scenariotools.smlk.examples.derating

import org.junit.jupiter.api.Test
import org.scenariotools.smlk.*

// to be extended by other components
abstract class Runnable{
    fun startCycle() = event{}
    fun endCycle() = event{}
}

class DeratingComponent : Runnable(){

    // IN / UNCONTROLLABLE
    var coolantTemp = 0
    var pcbTemp = 0
    fun setCoolantTemperature(temp : Int) = event(temp){this.coolantTemp = temp}
    fun setPcbTemperature(temp : Int) = event(temp){this.pcbTemp = temp}

    // OUT / CONTROLLABLE
    fun setDeratingFactor(deratingFactor : Double) = event(deratingFactor){ this.deratingFactor = deratingFactor}
    var deratingFactor = 0.0 // values [0..1]
    fun exceptionalTemperatureIncrease() = event{}
}


fun deratingGuaranteeScenarios(deratingComponent: DeratingComponent) = listOf(

    // EndCycle must happen between two startCycle
    scenario(deratingComponent.startCycle()){
        interruptingEvents.add(deratingComponent.endCycle())
        waitFor(deratingComponent.startCycle())
        violation("endCycle must happen between two startCycle")
    },

    // EndCycle happens after startCycle
    scenario(deratingComponent.startCycle()){
        request(deratingComponent.endCycle())
    },


    // The derating factor must be set in each cycle
    cycleScenario(deratingComponent){
        interruptingEvents.add(deratingComponent.exceptionalTemperatureIncrease())
        mustOccur(deratingComponent receives DeratingComponent::setDeratingFactor)
    },

    // Derating factor is 1.0 in normal operation temperature range [-40°..65°]
    cycleScenario(deratingComponent){
        interruptingEvents.add(deratingComponent.exceptionalTemperatureIncrease())
        if (deratingComponent.coolantTemp in -40..65)
            request(deratingComponent.setDeratingFactor(1.0))
    },

    // Derating factor is 0.0 if temperature < -40
    cycleScenario(deratingComponent){
        interruptingEvents.add(deratingComponent.exceptionalTemperatureIncrease())
        if (deratingComponent.coolantTemp < -40)
            request(deratingComponent.setDeratingFactor(0.0))
    },

    // Derating factor is 0.0 if temperature > -75
    cycleScenario(deratingComponent){
        interruptingEvents.add(deratingComponent.exceptionalTemperatureIncrease())
        if (deratingComponent.coolantTemp > 75)
            request(deratingComponent.setDeratingFactor(0.0))
    },

    // Derating factor derages linearly from 1.0 to 0.0 between [65..75]
    cycleScenario(deratingComponent){
        interruptingEvents.add(deratingComponent.exceptionalTemperatureIncrease())
        if (deratingComponent.coolantTemp in 65..75)
            request(deratingComponent.setDeratingFactor((75.0-deratingComponent.coolantTemp)/10))
    },

    // Shutdown if temperature increases more than 5 degrees within a period of 5 seconds
    scenario(deratingComponent receives DeratingComponent::setCoolantTemperature){

        // scenario is initialized when a new coolant temperature value is set
        val initialTemp = it.parameters[0] as Int
        var currentTemp = initialTemp

        // scenario from hereon must sync with cycles start/end only where indicated below.
        forbiddenEvents.add(deratingComponent.startCycle())
        forbiddenEvents.add(deratingComponent.endCycle())

        for(i in 1..50){

            waitForOverrideLocalForbidden(deratingComponent.startCycle())

            if (currentTemp - initialTemp > 5){
                forbiddenEvents.add(deratingComponent receives DeratingComponent::setDeratingFactor)
                request(deratingComponent.exceptionalTemperatureIncrease())
                requestOverrideLocalForbidden(deratingComponent.setDeratingFactor(0.0))
            }

            waitForOverrideLocalForbidden(deratingComponent.endCycle())

            currentTemp = waitFor(deratingComponent receives DeratingComponent::setCoolantTemperature).parameters[0] as Int
        }
    },


    // Shutdown if pcb temperature increases more than 20 degrees within a period of 3 seconds
    scenario(deratingComponent receives DeratingComponent::setPcbTemperature){

        // scenario is initialized when a new temperature value is set
        val initialPcbTemp = it.parameters[0] as Int

        // wait until the end of the cycle in which the new pcb temperature was set
        waitFor(deratingComponent.endCycle())

        // scenario from hereon must sync with cycles start/end only where indicated below.
        forbiddenEvents.add(deratingComponent.startCycle())
        forbiddenEvents.add(deratingComponent.endCycle())


        for(i in 2..30){
            val pcbTemp = waitFor(deratingComponent receives DeratingComponent::setPcbTemperature).parameters[0] as Int

            waitForOverrideLocalForbidden(deratingComponent.startCycle())

            if (pcbTemp - initialPcbTemp > 20){
                forbiddenEvents.add(deratingComponent receives DeratingComponent::setDeratingFactor)
                request(deratingComponent.exceptionalTemperatureIncrease())
                requestOverrideLocalForbidden(deratingComponent.setDeratingFactor(0.0))
                break // end for loop and, hence, the scenario
            }

            waitForOverrideLocalForbidden(deratingComponent.endCycle())
        }
    }

)

/**
 * Creates a scenario that is triggered by the startCycle event received by the Runnable <code>runnable</code>.
 * The scenario <code>mainScenario</code> is then executed, followed by <code>runnable.endCycle()</code>.
 * For the duration of its execution, further <code>runnable.startCycle()</code> and <code>runnable.endCycle()</code>
 * events are forbidden, i.e., the behavior of mainScenario must take place within one cycle.
 */
fun cycleScenario(runnable: Runnable, mainScenario : suspend Scenario.() -> Unit) : suspend Scenario.() -> Unit =
    scenario(runnable.startCycle()){
        forbiddenEvents.add(runnable.startCycle() union runnable.endCycle())
        mustProgress {
            mainScenario.invoke(this@scenario)
            requestOverrideLocalForbidden(runnable.endCycle())
        }
    }


fun createDeratingScenarioProgram(deratingComponent: DeratingComponent) : ScenarioProgram {
    val deratingScenarioProgram = ScenarioProgram(
        "DeratingScenarioProgram",
        isControlledObject = { o : Any -> o is DeratingComponent }, // ObjectEvent side effects of DeratingComponents, e.g. setCoolantTemperature(...), will only be executed by the DeratingScenarioProgram, and not the test program.
        eventNodeInputBufferSize = 25)

    deratingScenarioProgram.addEnvironmentMessageType(
        Runnable::startCycle,
        DeratingComponent::setCoolantTemperature,
        DeratingComponent::setPcbTemperature
    )

    deratingScenarioProgram.activeGuaranteeScenarios.addAll(deratingGuaranteeScenarios(deratingComponent))

    return deratingScenarioProgram
}

class DeratingTest {

    val deratingComponent = DeratingComponent()
    val deratingScenarioProgram = createDeratingScenarioProgram(deratingComponent)

    @Test
    fun `At 0°C the DeratingFactor is 1-0`() = runTest(deratingScenarioProgram){
        deratingIOSequence(Triple(0, 0, 1.0))
    }
    @Test
    fun `At 1°C the DeratingFactor is 1-0`() = runTest(deratingScenarioProgram){
        deratingIOSequence(Triple(1, 1, 1.0))
    }
    @Test
    fun `At -1°C the DeratingFactor is 1-0`() = runTest(deratingScenarioProgram){
        deratingIOSequence(Triple(-1, -1, 1.0))
    }
    @Test
    fun `At -40°C the DeratingFactor is 1-0`() = runTest(deratingScenarioProgram){
        deratingIOSequence(Triple(-40, -40, 1.0))
    }
    @Test
    fun `At 65°C the DeratingFactor is 1-0`() = runTest(deratingScenarioProgram){
        deratingIOSequence(Triple(65, 65, 1.0))
    }

    @Test
    fun `At 66°C the DeratingFactor is 0-9`() = runTest(deratingScenarioProgram){
        deratingIOSequence(Triple(66, 66, 0.9))
    }
    @Test
    fun `At -41°C the DeratingFactor is 0-0`() = runTest(deratingScenarioProgram){
        deratingIOSequence(Triple(-41, -41, 0.0))
    }
    @Test
    fun `At 75°C the DeratingFactor is 0-0`() = runTest(deratingScenarioProgram){
        deratingIOSequence(Triple(-41, -41, 0.0))
    }

    @Test
    fun `At 70°C the DeratingFactor is 0-5`() = runTest(deratingScenarioProgram){
        deratingIOSequence(Triple(70, 70, 0.5))
    }

    @Test
    fun `At 71°C the DeratingFactor is 0-4`() = runTest(deratingScenarioProgram){
        deratingIOSequence(Triple(71, 71, 0.4))
    }


    // Tests for Requirement:
    // Shutdown if temperature increases more than 5 degrees within a period of 5 seconds


    @Test
    fun `Temperature increases 5 degrees from 10 to 15 within a period of 5 seconds starting from 10°C`() = runTest(deratingScenarioProgram){
        deratingIOSequence(
            Triple(10, 10, 1.0), // 0 - .1 sec
            *9 times Triple(11, 11,  1.0), // .1 - 1 sec
            *10 times Triple(12,12,1.0), // 2 sec
            *10 times Triple (13, 13, 1.0), // 3 sec
            *10 times Triple (14, 14,1.0), // 4 sec
            *10 times Triple (15, 15, 1.0)  // 4 - 4.9 sec
        )
    }

    @Test
    fun `Temperature increases 6 degrees from 10 to 16 within a period of 5 seconds starting from 10°C`() = runTest(deratingScenarioProgram){
        deratingIOSequence(
            Triple(10, 10, 1.0), // 0 - .1 sec
            *9 times Triple(11, 11,  1.0), // .1 - 1 sec
            *10 times Triple(12,12,1.0), // 2 sec
            *10 times Triple (13, 13, 1.0), // 3 sec
            *10 times Triple (14, 14,1.0), // 4 sec
            *9 times Triple (15, 15, 1.0),  // 4 - 4.9 sec
            Triple(16, 16, 0.0)  // 4.9 - 5 sec, exceptional temperature increase, must set derating factor to 0.0
        )
    }

    // Tests for Requirement:
    // Shutdown if pcb temperature increases more than 20 degrees within a period of 3 seconds

    @Test
    fun `pcb Temperature increases 21 degrees from 20 to 41 within a period of 3 seconds starting from 20°C`() = runTest(deratingScenarioProgram){
        deratingIOSequence(
            Triple(10, 20, 1.0), // 0 - .1 sec
            *9 times Triple(11, 21,  1.0), // .1 - 1 sec
            *10 times Triple(12,30,1.0), // 2 sec
            *9 times Triple (13, 30, 1.0), // 2.9 sec
            Triple(14, 41, 0.0)  // 2.9 - 3 sec, exceptional temperature increase, must set derating factor to 0.0


        )
    }

    // Helper methods

    suspend fun Scenario.deratingIOSequence(vararg inOutValues: Triple<Int, Int, Double>){
        for(inOutValue in inOutValues){
            request(deratingComponent.setCoolantTemperature(inOutValue.first))
            request(deratingComponent.setPcbTemperature(inOutValue.second))
            request(deratingComponent.startCycle())
            val symbolicSetDeratingFactorEvent = deratingComponent receives DeratingComponent::setDeratingFactor
            val expectedSetDeratingFactorEvent = deratingComponent.setDeratingFactor(inOutValue.third)
            val otherThanExpectedSetDeratingFactorEvent = symbolicSetDeratingFactorEvent excluding expectedSetDeratingFactorEvent
            waitFor(expectedSetDeratingFactorEvent, otherThanExpectedSetDeratingFactorEvent)
            waitFor(deratingComponent.endCycle())
        }
    }

    infix fun <A,B>Int.times(pair: Pair<A,B>) = Array(this) {pair}
    infix fun <A,B,C>Int.times(triple: Triple<A,B,C>) = Array(this) {triple}

}