package org.scenariotools.smlk.examples.symbolicrealtime.robotcell1

import org.scenariotools.smlk.event

abstract class NamedElement(val name : String){
    override fun toString(): String {
        return name
    }
}

class Location(name : String) : NamedElement(name)

val locA = Location("Loc A")
val locB = Location("Loc B")
val locC = Location("Loc C")
val locD = Location("Loc D")


class Robot(name : String, var location : Location) : NamedElement(name) {

    fun moveTo(newLocation : Location) = event(newLocation){
        println("Robot $name shall move to location $newLocation")
    }

}

val r1 = Robot("r1", locA)
val r2 = Robot("r2", locB)

val movementTimes = mapOf<Location, Map<Location, Double>>(
    locA to mapOf(
        locC to 3.0
    ),
    locB to mapOf(
        locD to 1.0
    )
)

object Controller : NamedElement("Controller") {

    fun arrived(robot: Robot, newLocation : Location, time : Double) = event(robot, newLocation, time){
        robot.location = newLocation
        println("Robot $robot arrived at location $newLocation at time $time")
    }

}