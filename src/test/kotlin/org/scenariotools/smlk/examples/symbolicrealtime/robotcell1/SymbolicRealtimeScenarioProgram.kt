package org.scenariotools.smlk.examples.symbolicrealtime.robotcell1

import kotlinx.coroutines.runBlocking
import org.scenariotools.smlk.ScenarioProgram
import org.scenariotools.smlk.examples.symbolicrealtime.*
import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.symbolicEvent


fun main() {
    val scenarioProgram = createSymbolicRealtimeScenarioProgram()
    runBlocking { scenarioProgram.run() }
}

fun createSymbolicRealtimeScenarioProgram () : ScenarioProgram{

    val scenarioProgram = ScenarioProgram()

    scenarioProgram.activeGuaranteeScenarios.addAll(
        listOf(
            scenario(Robot::moveTo.symbolicEvent()){
                val robot = it.receiver
                val newLocation = it.parameters[0] as Location
                val movementTime = movementTimes[robot.location]?.get(newLocation) ?: error("")
                val movementWaitMessage = "waiting for robot $robot to reach location $newLocation"
                wait(movementTime, waitMessage = movementWaitMessage)
                // 20 % probability that robot movement is interrupted
                withProbability(0.2){
                    println("Robot $robot is interrupted")
                    // meanWaitingTime: 60 seconds
                    requestAfterRandomTimeWithExpDistribution(
                        60.0,
                        Controller.arrived(robot, newLocation, clock.time()),
                        forbiddenEvents = anyWaiting,
                        waitMessage = movementWaitMessage)
                }
            },
            scenario {
                request(r1.moveTo(locC), forbidden = anyWaiting)
                request(r2.moveTo(locD), forbidden = anyWaiting)
            }
        )
    )

    return scenarioProgram
}