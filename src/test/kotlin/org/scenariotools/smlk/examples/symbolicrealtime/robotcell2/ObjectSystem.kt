package org.scenariotools.smlk.examples.symbolicrealtime.robotcell2

import org.scenariotools.smlk.*
import javax.naming.Name
import kotlin.math.sqrt

abstract class NamedElement(val name : String){
    override fun toString() = name
    override fun hashCode() = "${this::class.qualifiedName}$name".hashCode()
    override fun equals(other: Any?) = this === other
            || (other != null && other is NamedElement && other::class == this::class && other.name == name)
}


class Robot(name : String) : NamedElement(name) {
    val homePosition = "home_$name"
    fun performTask(taskName : String) = event(taskName){}
    fun returnToHomePosition() = event(){}
}

class Environment(name : String) : NamedElement(name) {
    fun cycleFinished(time : Double, message : String) = event(time, message){}
}

val robotA = Robot("robotA")
val robotB = Robot("robotB")
val robotC = Robot("robotC")
val robotD = Robot("robotD")

val robotsToTasksMap = mapOf(
    robotA to setOf("A", "B", "C", "D", "E"),
    robotB to setOf("D", "E", "F", "G", "H"),
    robotC to setOf("G", "H", "I", "J", "K"),
    robotD to setOf("J", "K", "L", "A", "B"),
)

val coordinates = mapOf(
    "A" to (0.0 to 2.0),
    "B" to (0.0 to 3.0),
    "C" to (1.0 to 4.0),
    "D" to (2.0 to 5.0),
    "E" to (3.0 to 5.0),
    "F" to (4.0 to 4.0),
    "G" to (5.0 to 3.0),
    "H" to (5.0 to 2.0),
    "I" to (4.0 to 1.0),
    "J" to (3.0 to 0.0),
    "K" to (2.0 to 0.0),
    "L" to (1.0 to 1.0),
    robotA.homePosition to (0.0 to 5.0),
    robotB.homePosition to (5.0 to 5.0),
    robotC.homePosition to (0.0 to 5.0),
    robotD.homePosition to (0.0 to 0.0),
)

val taskTimes = mapOf(
    "A" to 1000.0,
    "B" to 1000.0,
    "C" to 1000.0,
    "D" to 1000.0,
    "E" to 1000.0,
    "F" to 1000.0,
    "G" to 1000.0,
    "H" to 1000.0,
    "I" to 1000.0,
    "J" to 1000.0,
    "K" to 1000.0,
    "L" to 1000.0,
)

const val meanRepairTime = 10000.0
const val meanTimeToNextRobotDamage = 20000.0


class Controller(name : String, val robotsToTasksMap : Map<Robot, Set<String>>) : NamedElement(name) {

    val finishedTasks = mutableSetOf<String>()
    val robotsEventLog = mutableMapOf<Robot, MutableList<String>>()
    val robotsToCurrentLocationMap = mutableMapOf<Robot, String>()
    val robotsToTargetLocationMap = mutableMapOf<Robot, String>()
    val robotsDamaged = mutableMapOf<Robot, Boolean>()
    init {
        robotsToTasksMap.keys.forEach { robot ->
            robotsDamaged[robot] = false
            robotsToCurrentLocationMap[robot] = robot.homePosition
            robotsToTargetLocationMap[robot] = ""
            robotsEventLog[robot] = mutableListOf()
        }
    }
    fun startCycle() = event(){
        finishedTasks.clear()
        robotsEventLog.keys.forEach{r -> robotsEventLog[r]!!.clear()}
    }
    fun robotArrivedAt(robot: Robot, task : String) = event(robot, task){
        robotsToCurrentLocationMap[robot] = task
        robotsEventLog[robot]!!.add("At $task")
    }
    fun robotArrivedAtHomePosition(robot: Robot) = event(robot){
        robotsToCurrentLocationMap[robot] = robot.homePosition
        robotsEventLog[robot]!!.add("HOME")
    }
    fun robotFinishedTask(robot : Robot, task : String) = event(robot, task){
        robotsEventLog[robot]!!.add("$task DONE")
        finishedTasks.add(task)
    }
    fun robotDamaged(robot : Robot) = event(robot){
        robotsEventLog[robot]!!.add("*Damaged*")
        robotsDamaged[robot] = true
    }
    fun robotRepaired(robot : Robot) = event(robot){
        robotsEventLog[robot]!!.add("*Repaired*")
        robotsDamaged[robot] = false
    }
    fun shutDown() = event(){}
    fun logOrderedRobotHome(robot : Robot) = event(robot){
        robotsEventLog[robot]!!.add("Go HOME")
        robotsToTargetLocationMap[robot] = robot.homePosition
    }
    fun logOrderedRobotPerformTask(robot : Robot, task : String) = event(robot, task){
        robotsEventLog[robot]!!.add("Do $task")
        robotsToTargetLocationMap[robot] = task
    }
    fun robotIsHome(robot : Robot) = robotsToCurrentLocationMap[robot]!! == robot.homePosition
}


class ProductionCell(name : String,
                     val robotsToTasksMap : Map<Robot, Set<String>>,
                     val coordinates : Map<String, Pair<Double, Double>>,
                     val taskTimes : Map<String, Double>,
                     val meanTimeToNextRobotDamage : Double,
                     val meanRepairTime : Double,
) : NamedElement(name) {

    val clock : Clock = Clock()

    val robots = robotsToTasksMap.keys

    val env = Environment("env")

    val controller = Controller("controller", robotsToTasksMap)

    fun movementTime(fromLocation : String, toLocation : String) : Double{
        val deltaX = coordinates[fromLocation]!!.first - coordinates[toLocation]!!.first
        val deltaY = coordinates[fromLocation]!!.second - coordinates[toLocation]!!.second
        return sqrt(deltaX*deltaX + deltaY*deltaY)*500
    }

    private fun <T>getEqualObject(o : T) : T? {
        return when(o){
            is Clock -> clock as T
            is Controller -> if (o == controller) controller as T else null
            is Environment -> if (o == env) env as T else null
            is Robot -> {
                for(r in robots){
                    if (o == r) return r as T
                }
                return null
            }
            else -> o
        }
    }


    fun mapObjectEvents(events : List<Event>) = events.map {
            e -> if (e is ObjectEvent<*,*>) mapObjectEvent(e) else e
    }

    fun <R:Any,T> mapObjectEvent(e : ObjectEvent<R,T>) : ObjectEvent<R,T>{
        val event = e.type.call(getEqualObject(e.receiver)!!, *e.parameters.map { getEqualObject(it)!! }.toTypedArray())
        e.sender?.let { return getEqualObject(it)!! sends event } ?: return event
    }

}
