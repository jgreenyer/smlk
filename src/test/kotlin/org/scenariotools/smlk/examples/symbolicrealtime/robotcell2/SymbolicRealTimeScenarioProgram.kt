package org.scenariotools.smlk.examples.symbolicrealtime.robotcell2

import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.scenariotools.smlk.*
import org.scenariotools.smlk.examples.symbolicrealtime.*
import kotlin.system.measureTimeMillis

fun main() {
    val eventRecorder = mutableListOf<Event>()
    val cell = ProductionCell("Cell", robotsToTasksMap, coordinates, taskTimes, 10000.0, meanRepairTime)
    val scenarioProgram = createRobotCellScenarioProgram("CellSP", cell, 1, eventRecorder)

    scenarioProgram.activeGuaranteeScenarios.add (
        scenario {
            val recordedEvents = mutableListOf<Event>()
            while (true){
                val event = waitFor(ALLEVENTS)
                if (startCycle.contains(event)){
                    recordedEvents.clear()
                }
                recordedEvents.add(event)
                if (event is ObjectEvent<*,*> && event.receiver is Controller){
                   val simCell = ProductionCell("SIMCell", robotsToTasksMap, coordinates, taskTimes, meanTimeToNextRobotDamage, meanRepairTime)
                    val internalScenarioProgram = createRobotCellScenarioProgram("SIMCellSP", simCell,1)
                    runBlocking {

                        internalScenarioProgram.runEvents(simCell.mapObjectEvents(recordedEvents))
//                        println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                        internalScenarioProgram.continueRun()
//                        println("-------")
                    }
                }
            }
        }
    )

    val executionTime = measureTimeMillis {
        runBlocking {
            scenarioProgram.run()
            println(scenarioProgram.clock.time())
        }
    }
    println("time: $executionTime")

//    replay(scenarioProgram, eventRecorder)
}

private fun replay(eventRecorder: MutableList<Event>) {
    val eventRecorder2 = mutableListOf<Event>()
    val cell = ProductionCell("OtherCell", robotsToTasksMap, coordinates, taskTimes, meanTimeToNextRobotDamage, meanRepairTime)
    val scenarioProgram = createRobotCellScenarioProgram("REPLAY RobotCell SP ", cell, eventRecorder = eventRecorder2)
    val reducedEventsList = eventRecorder.subList(0, eventRecorder.size - 10)


    runBlocking {
        //scenarioProgram.runEvents(eventRecorder)
        scenarioProgram.runEvents(reducedEventsList)
        println(scenarioProgram.clock.time())
        //scenarioProgram.continueRun()
    }

    for (i in 0 until maxOf(eventRecorder.size, eventRecorder2.size)) {
        if (i < eventRecorder.size)
            println("1ST - $i: ${eventRecorder[i]}")
        else
            println("1ST - $i: no event")
        if (i < eventRecorder2.size)
            println("2ND - $i: ${eventRecorder2[i]}")
        else
            println("2ND - $i: no event")
        println("-----")
    }
}

val logger = KotlinLogging.logger("RobotCellLogger")

fun createRobotCellScenarioProgram (name : String, cell : ProductionCell, numberOfCyclesToExecute : Int = 1, eventRecorder : MutableList<Event>? = null) : ScenarioProgram{

    val scenarioProgram = ScenarioProgram(name, clock = cell.clock)

    if (eventRecorder != null)
        scenarioProgram.eventRecorder = eventRecorder

    scenarioProgram.addEnvironmentMessageType(
        Controller::startCycle,
        Controller::robotArrivedAt,
        Controller::robotArrivedAtHomePosition,
        Controller::robotFinishedTask,
        Controller::robotDamaged,
        Controller::robotRepaired,
        Controller::shutDown,
        Clock::wait)

    val controller = cell.controller
    val env = cell.env

    scenarioProgram.activeGuaranteeScenarios.addAll(
        createRobotGuaranteeScenarios(controller, env)
    )

    scenarioProgram.activeGuaranteeScenarios.addAll(
        listOf(
            scenario(){
                val allTasks = controller.robotsToTasksMap.values.flatten().toSet()
                while (true){
                    waitFor(Controller::robotFinishedTask.symbolicEvent()
                            union Controller::robotArrivedAtHomePosition.symbolicEvent())
                    if (controller.finishedTasks.containsAll(allTasks)
                        && controller.robotsToTasksMap.keys.all { controller.robotsToCurrentLocationMap[it] == it.homePosition })
                        request(controller sends env.cycleFinished(clock.time(), "Total cycle time: ${clock.time()}, ${controller.robotsEventLog}"))
                }
            }
        )
    )

    scenarioProgram.activeAssumptionScenarios.addAll(
        createRobotAssumptionScenarios(cell, controller, env)
    )

    scenarioProgram.activeAssumptionScenarios.add(
        scenario{
            repeat(numberOfCyclesToExecute){
                urgent {
                    request(env sends controller.startCycle())
                }
                waitFor(cycleFinished)
            }
            request(env sends controller.shutDown())
        }
    )

    return scenarioProgram
}

val cycleFinished = Environment::cycleFinished.symbolicEvent()
val startCycle = Controller::startCycle.symbolicEvent()
val shutDown = Controller::shutDown.symbolicEvent()


fun createRobotAssumptionScenarios(cell : ProductionCell, controller : Controller, env: Environment) : Set<suspend Scenario.() -> Unit>{
    val robotScenarioSet = mutableSetOf<suspend Scenario.() -> Unit>()
    for (robot in controller.robotsToTasksMap.keys){
        robotScenarioSet.addAll(createRobotAssumptionScenarios(cell, robot, controller, env))
    }
    return robotScenarioSet
}

fun createRobotAssumptionScenarios(cell : ProductionCell, robot : Robot, controller: Controller, env : Environment) = setOf(
    // When a robot is assigned a task, it moves to the task location,
    scenario(controller sends robot receives Robot::performTask) {
        interruptingEvents.add(env sends controller.robotDamaged(robot))
        val task = it.parameters[0] as String
        val movementTime = cell.movementTime(controller.robotsToCurrentLocationMap[robot]!!, task)
        val meanExtraMovementTime = movementTime / 10
        val movementWaitMessage = "waiting for $robot to reach location of task $task"
        wait(movementTime, waitMessage = movementWaitMessage)
        requestAfterRandomTimeWithExpDistribution(
            meanWaitingTime = meanExtraMovementTime,
            requestEvents = robot sends controller.robotArrivedAt(robot, task),
            waitMessage = movementWaitMessage )
        //then it signals the controller about arriving at the task location,
        val taskTime = cell.taskTimes[task]!!
        val meanExtraTaskTime = taskTime / 10
        val taskWaitMessage = "waiting for $robot to finish task $task"
        // then it performs the task, and signals the controller when it finished the task.
        wait(taskTime, waitMessage = taskWaitMessage)
        requestAfterRandomTimeWithExpDistribution(
            meanWaitingTime = meanExtraTaskTime,
            robot sends controller.robotFinishedTask(robot, task),
            waitMessage = taskWaitMessage)
    },
    // When a robot is requested to move back to the home location, it will move to the home location.
    scenario(controller sends robot.returnToHomePosition()){

        // interrupt this scenario if the robot already returned home
        interruptingEvents.add(robot sends controller.robotArrivedAtHomePosition(robot))

        if (controller.robotsToCurrentLocationMap[robot] != robot.homePosition) {
            // if the robot is not home, then after a certain movement time it will arrive home
            val movementTime = cell.movementTime(controller.robotsToCurrentLocationMap[robot]!!, robot.homePosition)
            val meanExtraMovementTime = movementTime / 10
            val movementWaitMessage = "waiting for $robot to reach the home position"
            wait(movementTime, waitMessage = movementWaitMessage)
            requestAfterRandomTimeWithExpDistribution(meanExtraMovementTime,
                robot sends controller.robotArrivedAtHomePosition(robot),
                waitMessage = movementWaitMessage)
        }else{
            // if the robot already is at the home position, it will take some time and then the robot will re-signal
            // arriving at the home position
            requestAfterRandomTimeWithExpDistribution(100.0,
                robot sends controller.robotArrivedAtHomePosition(robot),
                waitMessage = "waiting for $robot to reply with signal that it is already home"
            )
        }
    },
    scenario(){
        shutDown interrupts {
            while (true){
                requestAfterRandomTimeWithExpDistribution(cell.meanTimeToNextRobotDamage,
                    env sends controller.robotDamaged(robot),
                    waitMessage = "waiting for $robot to be damaged")
                waitFor{controller.robotsToCurrentLocationMap[robot] == robot.homePosition}
                requestAfterRandomTimeWithExpDistribution(cell.meanRepairTime,
                    env sends controller.robotRepaired(robot),
                    waitMessage = "waiting for $robot to be repaired")
                waitFor(controller sends robot receives Robot::performTask)
            }
        }
    }
)

fun createRobotGuaranteeScenarios(controller : Controller, env : Environment) : Set<suspend Scenario.() -> Unit>{
    val robotScenarioSet = mutableSetOf<suspend Scenario.() -> Unit>()
    for (robot in controller.robotsToTasksMap.keys){
        robotScenarioSet.addAll(createRobotGuaranteeScenarios(robot, controller, env))
    }
    return robotScenarioSet
}

fun createRobotGuaranteeScenarios(robot : Robot, controller: Controller, env : Environment): Set<suspend  Scenario.() -> Unit>{

    val robotDamaged = env sends controller.robotDamaged(robot)
    val robotRepaired = env sends controller.robotRepaired(robot)
    val anyRobotFinishesTask = Controller::robotFinishedTask.symbolicEvent()

    return setOf(
        // When the cycle starts,
        //   or when some robot finished a task,
        //   or when the robot arrived at its home position,
        //   or when the robot is repaired from a damage
        // then each robot is assigned one of its unfinished tasks,
        //   or it is ordered to its home position.
        scenario(startCycle
              union anyRobotFinishesTask
              union (robot sends controller.robotArrivedAtHomePosition(robot))
              union robotRepaired) {

//            logger.info("--------")
//            logger.info("finished tasks : ${controller.finishedTasks}")
//            for (r in robotsToTasksMap.keys)
//                logger.info("  tasks performed by robot $r: ${controller.robotsEventLog[r]}")

          val possibleTaskAssignmentEvents = controller.createRobotTaskAssignmentEvents(robot)
          if (possibleTaskAssignmentEvents.isNotEmpty() && !controller.robotsDamaged[robot]!!){
                  interruptingEvents.add(cycleFinished union robotDamaged union anyRobotFinishesTask)
                  request(possibleTaskAssignmentEvents)
          }
        },
        // when a robot is assigned a task, it must not be assigned a new task or sent home until the task is finished or
        // the robot is damaged
        scenario (controller sends robot receives Robot::performTask) {
            val task = it.parameters[0] as String
            forbiddenEvents.add(controller sends robot.returnToHomePosition() union
                    (controller sends robot receives Robot::performTask))
            waitFor(robot sends controller.robotFinishedTask(robot, task) union robotDamaged)
        },
        //When a robot is sent home, it must not be sent home again before it is assigned another task
        scenario(controller sends robot.returnToHomePosition()){
            waitFor(controller sends robot receives Robot::performTask,
            forbidden = controller sends robot.returnToHomePosition())
        },
        // When a robot is assigned a task, other robots must not be assigned the same task
        // until the cycle ends or the robot is damaged (and must therefore abort the task).
        scenario(controller sends robot receives Robot::performTask){
          val task = it.parameters[0] as String
          for(anyRobot in controller.robotsToTasksMap.keys) {
              forbiddenEvents.add(controller sends anyRobot.performTask(task))
          }
          waitFor(cycleFinished union robotDamaged)
        },
        // When a robot finished a task, the task must not be assigned again to any robot
        // until the cycle ends.
        scenario(robot sends controller receives Controller::robotFinishedTask){
          val task = it.parameters[1] as String
          for(anyRobot in controller.robotsToTasksMap.keys) {
              forbiddenEvents.add(controller sends anyRobot.performTask(task))
          }
          waitFor(cycleFinished)
        },
//        // If a robot arrives at the home position, it must not be resent to the home position
//        // before being sent to perform another task
//        scenario(robot sends controller.robotArrivedAtHomePosition(robot)){
//          waitFor(
//              controller sends robot receives Robot::performTask
//                      union robotDamaged,
//              forbidden = controller sends robot.returnToHomePosition())
//        },
        // Forbid a robot to be sent back to its home position if there are still unfinished tasks that can be allocated to that robot.
        scenario(startCycle union robotRepaired){
             name = "unless damaged, do not send robot home when there are unfinished tasks (${controller.finishedTasks} -- ${controller.robotsToTasksMap[robot]})"
          scenario{
              interruptingEvents.add(env sends controller.robotDamaged(robot))
              waitFor { controller.finishedTasks.containsAll(
                  controller.robotsToTasksMap[robot]!!) }
          } before (controller sends robot.returnToHomePosition())
        },
        // When a robot is damaged it must not be assigned any task until it is repaired
        scenario(robotDamaged){
            waitFor(robotRepaired,
                forbidden = controller sends robot receives Robot::performTask)
        },
        // If a robot is damaged, then, if the robot is not currently at its home position
        // or already on its way to the home location, the controller must send the robot to its home position.
        scenario(robotDamaged){
          if (!controller.robotIsHome(robot) && controller.robotsToTargetLocationMap[robot] != robot.homePosition){
              request(controller sends robot.returnToHomePosition())
          }
        },
        // If a robot is damaged, then it must not be assigned a new task before
        // the robot is repaired
        scenario(env sends controller.robotDamaged(robot)){
          waitFor(env sends controller.robotRepaired(robot),
              forbidden = controller sends robot receives Robot::performTask)
        },
        // log event of controller sending robot to home position
        scenario(controller sends robot.returnToHomePosition()) {
          commit(controller.logOrderedRobotHome(robot))
        },
        // log event of controller assigning a task to a robot
        scenario(controller sends robot receives Robot::performTask) {
          commit(controller.logOrderedRobotPerformTask(robot, it.parameters[0] as String))
        },
    )
}

fun Controller.createRobotTaskAssignmentEvents(robot : Robot) : ConcreteEventSet {
    val robotTasks = mutableSetOf<String>()
    robotTasks.addAll(robotsToTasksMap[robot]!!)
    robotTasks.removeAll(finishedTasks)
    val possibleAssignTaskEvents = mutableSetOf<ObjectEvent<*, *>>()
    for (task in robotTasks){
        possibleAssignTaskEvents.add(this sends robot.performTask(task))
    }
    if (!robotIsHome(robot))
        possibleAssignTaskEvents.add(this sends robot.returnToHomePosition())
    return ConcreteEventSet(possibleAssignTaskEvents)
}




/*

ASSUMPTIONS:
1. (Implicit condition) Initially, the robots are at their base position
2. When a robot is assigned a task, it moves to the task location,
   then it signals the controller about arriving at the task location,
   then it performs the task, and signals the controller when it finished the task.
   The duration of the movement and performing the task requires a certain movement time + task time.
3. When a robot is requested to move back to the home location, it will move to the home location.
   When arriving at the home location after some movement time, it will signal the controller about
   arriving at the home location.
4. When a robot is assigned a task, it may be damaged/interrupted (while moving to the task location or performing
   the task). When this happens, the environment signals the controller to move the robot back to the home position.
   After arriving at the home position, the robot is repaired by a worker. When the repair is finished, the environment
   signals the controller that the robot is repaired and can commence working.


GUARANTEES:
1. When the cycle starts, each robot is assigned an (unfinished/unassigned) task from its list of possible tasks.
2. When a robot is assigned a task, the task is marked as assigned.
3. When a robot finishes a task, that task is marked as finished
3. When a robot finishes a task, the robot is assigned a new task from its
   list of possible tasks, or the robot is ordered to return to the home position.
3. A robot must not be assigned a task that is already finished
4. A robot must not be assigned a task that is already assigned to another robot.
5. When the robot reaches its home position, it may remain there or it may be assigned a new (unfinished/unassigned)
   task from its list of possible tasks.
MOVEMENT CONSTRAINTS:
6. When a robot moves from one task location to another, this may constrain what tasks may be assigned to a robot.
   because with the resulting movement it may collide with the other robot.
   There is a map that maps (StartLoc_RA, TargetLoc_RA, StartLoc_RB, TargetLoc_RB) -> Potential Collision? (TRUE/FALSE).
DAMAGE:
7. When the environment signals the controller that a robot is damaged, the controller must signal the robot to move
   back to its home position.
   If the robot is assigned a task, that task must again be marked as unassigned.
8. When the environment signals the controller that a robot arm is repaired, the robot can be
   assigned a new (unfinished/unassigned) task from its list of possible tasks.
9. When a robot arrived at its home position and a task is marked as unassigned, the robot may be assigned with that
   task if the task belongs to the list of possible tasks for that robot.



* */

//class TimedScenarioProgramTest{
//
//    val scenarioProgram = createTimedScenarioProgram()
//
//    @Test
//    fun testTimedScenarioProgram() = runTest(scenarioProgram, timeoutMillis=8000){
//        repeat(1){
//            logger.info("Starting cycle")
////            val currentTime = Date().time
////            suspendForMillis(100)
//            request(env sends controller.startCycle())
////            logger.info ("Scenario suspended ${Date().time - currentTime} before doing tick")
//        }
//    }
//
//
//}
