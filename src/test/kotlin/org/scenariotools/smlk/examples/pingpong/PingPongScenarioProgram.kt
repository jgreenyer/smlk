package org.scenariotools.smlk.examples.pingpong

import kotlinx.coroutines.runBlocking
import org.scenariotools.smlk.*


fun main () {

    val scenarioProgram = ScenarioProgram()

    scenarioProgram.addEnvironmentMessageType(PongMe::start)

    scenarioProgram.activeAssumptionScenarios.add{
        println("Starting")
        println("requesting start")
        request(PongMe.start())
    }

    scenarioProgram.activeGuaranteeScenarios.add(scenario(PongMe.start()) {
        println("Ping requesting ping(1) ** starting :)")
        request(PingMe.ping(1))

        interruptingEvents.add(PongMe receives  PongMe::pong param listOf(101..200, ANY))

        while (true){
            val lastPongMessageEvent = waitFor(SymbolicObjectEvent(PongMe, PongMe::pong, 1..100, ANY))
            val i = ((lastPongMessageEvent.parameters[0] as Int) + (lastPongMessageEvent.parameters[1]) as Int)
            println("Ping requesting ping($i)")
            request(PingMe.ping(i))
        }
    })

    scenarioProgram.activeGuaranteeScenarios.add(
        scenario(PingMe receives PingMe::ping param ("x" to 1..200)){
            val i = (it.parameters[0] as Int)
            println("Pong requesting pong($i, ${i+1})")
            request(PongMe.pong(i, i+1))
            println("Pong terminating")
        }
    )

    runBlocking { scenarioProgram.run() }


}