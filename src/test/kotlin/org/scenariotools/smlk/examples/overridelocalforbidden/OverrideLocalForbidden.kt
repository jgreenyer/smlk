package org.scenariotools.smlk.examples.overridelocalforbidden

import org.junit.jupiter.api.Test
import org.scenariotools.smlk.*

// to be extended by other components
object Obj{

    fun envEvent() = event{}
    fun a() = event{}
    fun b() = event{}
    fun c() = event{}

    override fun toString(): String {
        return "Obj"
    }
}

class OverrideLocalForbiddenTest {

    val scenarioProgram = ScenarioProgram(
        "OverrideLocalForbiddenTestScenarioProgram",
        isControlledObject = { o : Any -> o is Obj }, // ObjectEvent side effects of DeratingComponents, e.g. setCoolantTemperature(...), will only be executed by the DeratingScenarioProgram, and not the test program.
        eventNodeInputBufferSize = 25)

    init {
        scenarioProgram.addEnvironmentMessageType(Obj::envEvent)
        scenarioProgram.activeGuaranteeScenarios.addAll(
            listOf(
                scenario(Obj.envEvent()) {
                    forbiddenEvents.add(Obj.b() union Obj.c())
                    mustProgress {
                        request(Obj.a())
                        requestOverrideLocalForbidden(Obj.b())
                        requestOverrideLocalForbidden(Obj.c())
                    }
                }
            )
        )
    }

    @Test
    fun Test() = runTest(scenarioProgram){
        request(Obj.envEvent())
    }

}