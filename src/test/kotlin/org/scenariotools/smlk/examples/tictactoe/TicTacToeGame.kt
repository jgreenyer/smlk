package org.scenariotools.smlk.examples.tictactoe

import org.junit.jupiter.api.Test
import org.scenariotools.smlk.*

class TicTacToeGame {

    @Test
    fun mainTest() = runTest(scenarioProgram){
        val board = Board()
        val terminatingEvents = Board::draw.symbolicEvent() union
            Board::xWon.symbolicEvent() union
            Board::oWon.symbolicEvent()
        while(true){
            val possibleXEvents = ConcreteEventSet(
                (0..8).filter { i -> board.markings[i] == "_" }
                    .map { i -> board.X(i) }.toSet())
            request(possibleXEvents)
            val event = waitFor(Board::O.symbolicEvent()
                    union terminatingEvents)
            if (terminatingEvents.contains(event)){
                break
            }
        }
    }

    val scenarios = listOf(
        // alternate X and O
        scenario(){
            while(true){
                waitFor(Board::X.symbolicEvent(), forbidden = Board::O.symbolicEvent())
                waitFor(Board::O.symbolicEvent(), forbidden = Board::X.symbolicEvent())
            }
        },
        // respond to X by placing an O
        scenario(Board::X){
            request(it.receiver.allOEvents())
        },
        // do not mark any field twice
        scenario(){
            while(true){
                val e = waitFor(Board::X.symbolicEvent() union Board::O.symbolicEvent())
                if (e is ObjectEvent<*,*>){
                    val markedField = e.parameters.get(0) as Int
                    val board = e.receiver as Board
                    forbiddenEvents.add(board.X(markedField))
                    forbiddenEvents.add(board.O(markedField))
                }
            }
        },
        // after every X, check if X has won or if game is terminated with a draw
        scenario(Board::X){
            checkWinningCondition(it.receiver)
        },
        // after every O, check if O has won or if game is terminated with a draw
        scenario(Board::O){
            checkWinningCondition(it.receiver)
        },
        // if middle position is not taken, mark it!
        scenario(Board::X){
            val board = it.receiver
            if (board.markings[4] == "_"){
                markO(board,4)
            }
        },
        // mark opposite corner
        scenario(Board::X) {
            val board = it.receiver
            if (board.markings[0] == "X" && board.markings[8] == "_"){
                markO(board,8, 4)
            } else if (board.markings[2] == "X" && board.markings[6] == "_") {
                markO(board,6, 4)
            } else if (board.markings[6] == "X" && board.markings[2] == "_") {
                markO(board,2, 4)
            } else if (board.markings[8] == "X" && board.markings[0] == "_"){
                markO(board,0, 4)
            }
        }
    )

    fun Board.allOEvents() = ConcreteEventSet(events = (0..8).map { i -> this.O(i) }.toSet())

    suspend fun Scenario.markO(board:Board, vararg fields : Int){
        val possibleMarkingEvents = ConcreteEventSet(fields.map { i -> board.O(i) }.toSet())
        request(possibleMarkingEvents, forbidden = board.allOEvents() excluding possibleMarkingEvents)
    }

    private suspend fun Scenario.checkWinningCondition(board : Board) {
        forbiddenEvents.add(board::X.symbolicEvent() union Board::O.symbolicEvent())
        if (board.hasXWon()){
            request(board.xWon())
        } else if (board.hasOWon()) {
            request(board.oWon())
        } else if (board.isFilled()){
            request(board.draw())
        }
    }

    val scenarioProgram = createScenarioProgram()

    fun createScenarioProgram() : ScenarioProgram {
        val scenarioProgram = ScenarioProgram()
        scenarioProgram.addEnvironmentMessageType(Board::X)
        scenarioProgram.activeGuaranteeScenarios.addAll(scenarios)
        return scenarioProgram
    }

    class Board{
        val markings = Array<String>(9) {"_"}

        fun X(field : Int) = event(field){
            markings[field] = "X"
            printGameState()
        }

        fun O(field : Int) = event(field){
            markings[field] = "O"
            printGameState()
        }

        fun printGameState() {
            println( "GAME STATE       FIELD NUMBERS \n" +
                    markings[0] + " " + markings[1] + " " + markings[2] + "            0 1 2\n" +
                    markings[3] + " " + markings[4] + " " + markings[5] + "            3 4 5\n" +
                    markings[6] + " " + markings[7] + " " + markings[8] + "            6 7 8\n" )
        }

        fun draw() = event(){ println("Game over. DRAW!") }
        fun xWon() = event(){ println("Game over. X HAS WON!") }
        fun oWon() = event(){ println("Game over. O HAS WON!") }

        fun isFilled() = (0..8).all { i -> markings[i] != "_"}
        fun hasXWon() = hasWon("X")
        fun hasOWon() = hasWon("Y")

        private fun hasWon(player : String) = rows.any { row -> row.all { i -> markings[i] == player } }
                    || columns.any { col -> col.all { i -> markings[i] == player } }
                    || diagonals.any { diag -> diag.all { i -> markings[i] == player } }

        private val rows = arrayOf(intArrayOf(0, 1, 2), intArrayOf(3, 4, 5), intArrayOf(6, 7, 8))
        private val columns = arrayOf(intArrayOf(0, 3, 6), intArrayOf(1, 4, 7), intArrayOf(2, 5, 8))
        private val diagonals = arrayOf(intArrayOf(0, 4, 8), intArrayOf(2, 4, 6))

        override fun toString() = "board"
    }




}