package org.scenariotools.smlk

import kotlin.reflect.KFunction
import kotlin.reflect.full.allSuperclasses
import kotlin.reflect.full.declaredFunctions

/**
 * Creates an object event with the calling method as message type.
 */
fun <R:Any,T> R.event(vararg args : Any, sideEffect : ()->T) :  ObjectEvent<R,T>{

    val methodName = Thread.currentThread().getStackTrace()[2].methodName

    // this convention must hold: function must be of return type ObjectEvent<T>
    @Suppress("UNCHECKED_CAST")
    val messageTypes = this::class.declaredFunctions.filter { it.name == methodName }
        .ifEmpty {
            this::class.allSuperclasses.flatMap { it.declaredFunctions.filter { it.name == methodName }
                }.toList()} as List<KFunction<ObjectEvent<R,T>>>

    var correspondingMessageType : KFunction<ObjectEvent<R,T>>? = null
    for (messageType in messageTypes){
        if (args.size == messageType.parameters.size-1) correspondingMessageType = messageType
    }

    if (correspondingMessageType == null) {
        throw IllegalArgumentException("No matching message type (KFunction) could be determined during object event creation. Did you pass all function parameters to the event creation method?: $messageTypes")
    }
    else{
        val messageEvent = ObjectEvent<R,T>(this, correspondingMessageType, *args, sideEffect = sideEffect)
        return messageEvent
    }
}


fun Collection<KFunction<*>>.containsKFunction(other:Any) : Boolean{
    for (k in this){
        if (k.kFunctionEquals(other)) return true
    }
    return false
}

fun KFunction<*>.kFunctionEquals(other:Any) : Boolean{
    if (this.hashCode() != other.hashCode()) return false
    if (!(other is KFunction<*>)) return false
    if (this.returnType != other.returnType) return false
    if (this.name != other.name) return false
    return true
}
