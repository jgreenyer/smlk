package org.scenariotools.smlk

import kotlinx.coroutines.*
import mu.KotlinLogging

private val logger = KotlinLogging.logger {}

fun createTestScenarioProgram(testee : ScenarioProgram, observedTesteeEvents : IEventSet, testScenarioProgramEventNodeInputBufferSize : Int) : ScenarioProgram{
    val testScenarioProgram = ScenarioProgram("Test Scenario Program",
        terminateUponGuaranteeViolation = false,
        eventNodeInputBufferSize = testScenarioProgramEventNodeInputBufferSize,
        isControlledObject = {o : Any -> !testee.isControlledObject(o)})
    testee.terminatingEvents.add(TestDoneEvent)
    testee.eventNode.registerSender(testScenarioProgram.eventNode, testee.environmentMessageTypes.symbolicEvents() union TestDoneEvent)
    testScenarioProgram.eventNode.registerSender(testee.eventNode, observedTesteeEvents excluding TestDoneEvent)

    testScenarioProgram.activeGuaranteeScenarios.addAll(testee.activeAssumptionScenarios)
    testee.activeAssumptionScenarios.clear()

    return testScenarioProgram
}

fun runTest(testee : ScenarioProgram,
            observedTesteeEvents : IEventSet = ComplementEventSet(testee.environmentMessageTypes.symbolicEvents()),
            expectAssumptionViolation : Boolean = false,
            expectGuaranteeViolation : Boolean = false,
            testScenarioProgramEventNodeInputBufferSize : Int = 25,
            timeoutMillis : Long = 3000,
            expectingTimeOut : Boolean = false,
            testScenario: suspend Scenario.() -> Unit) {
    val testee_terminateUponAssumptionViolation = testee.terminateUponAssumptionViolation
    val testee_terminateUponGuaranteeViolation = testee.terminateUponGuaranteeViolation
    testee.terminateUponAssumptionViolation = false
    testee.terminateUponGuaranteeViolation = false

    val testScenarioProgram = createTestScenarioProgram(testee, observedTesteeEvents, testScenarioProgramEventNodeInputBufferSize)
    testScenarioProgram.activeGuaranteeScenarios.add(
        scenario{
            if (testee.superStepTerminationEvent != null){
                waitFor(testee.superStepTerminationEvent) // wait for the initializing superstep to finish
            }
            testScenario.invoke(this@scenario)
            request(TestDoneEvent)
        }
    )


    runBlocking{
        var testJob : Job? = null
        var testeeJob : Job? = null

        fun printTimeOutMessage() : String {
            return "${testJob!!.isCompleted}, testee terminated: ${testeeJob!!.isCompleted}; " +
                    "\n Guarantee scenarios in must-progress section: ${testee.guaranteeScenariosInMustProgressSection()}" +
                    "\n Assumption scenarios in must-progress section: ${testee.assumptionScenariosInMustProgressSection()}"
        }

        val timeOutJob =  launch {
            delay(timeoutMillis)
            if (!(testeeJob!!.isCompleted && testJob!!.isCompleted)){
                testeeJob!!.cancel()
                testJob!!.cancel()
                if (expectingTimeOut) {
                    logger.debug { "Timeout, as expected: ${printTimeOutMessage()}" }
                } else  {
                    throw RuntimeException("Timeout -- test failed: ${printTimeOutMessage()}")
                }
            }
        }

        testeeJob = launch {
            testee.run()
            if (testJob!!.isCompleted) timeOutJob.cancel()
        }
        testJob = launch {
            testScenarioProgram.run()
            if (testeeJob.isCompleted) timeOutJob.cancel()
        }
    }


    if (testee.guaranteeViolationOccurred() && !expectGuaranteeViolation){
        logger.debug { "An unexpected guarantee violation occurred." }
        throw testee.CompositeViolationException()
    }

    if (testee.assumptionViolationOccurred() && !expectAssumptionViolation){
        logger.debug { "An unexpected assumption violation occurred." }
        throw testee.CompositeViolationException()
    }

    if (testee.violationOccurred())
        logger.debug { "Violations occurred: ${testee.CompositeViolationException()}"}

    testee.terminatingEvents.remove(TestDoneEvent)
    testee.terminateUponAssumptionViolation = testee_terminateUponAssumptionViolation
    testee.terminateUponGuaranteeViolation = testee_terminateUponGuaranteeViolation
}

object TestDoneEvent: Event() {
    override fun toString() = "Test Done."
}

