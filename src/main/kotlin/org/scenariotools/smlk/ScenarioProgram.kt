package org.scenariotools.smlk

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import mu.KotlinLogging
import org.scenariotools.smlk.examples.symbolicrealtime.anyWaiting
import kotlin.reflect.KFunction


private val logger = KotlinLogging.logger {}

open class AbstractScenarioSyncMessage()
data class TerminatingScenarioMessage(val sender: Scenario, val violationException : ViolationException? = null) : AbstractScenarioSyncMessage()
data class SuspendMessage(val sender: Scenario, val forbiddenEvents: IEventSet) : AbstractScenarioSyncMessage()

data class ScenarioSyncMessage(
    val sender: Scenario,
    val waitedForEvents: IEventSet,
    val committedEvents: IConcreteEventSet,
    val requestedEvents: IConcreteEventSet,
    val forbiddenEvents: IEventSet,
    val toggleMustProgressState : Boolean) : AbstractScenarioSyncMessage()

data class AddActiveScenarioMessage(val sender: Scenario, val scenarioMain: suspend Scenario.() -> Unit) : AbstractScenarioSyncMessage()
data class AddTriggeredScenarioMessage<R:Any, T>(val sender: Scenario, val initObjectEvent : ObjectEvent<R, T>, val scenarioMain: suspend Scenario.(ObjectEvent<R, T>) -> Unit) : AbstractScenarioSyncMessage()
fun scenario(scenarioMain: suspend Scenario.() -> Unit) = scenarioMain

fun <R:Any, T> scenario(kFunction : KFunction<ObjectEvent<R,T>>, triggeredScenario: suspend Scenario.(ObjectEvent<R, T>) -> Unit): suspend Scenario.() -> Unit = {
    while(true){
        waitFor(SymbolicObjectEvent(type=kFunction)).launchScenario(triggeredScenario)
    }
}

fun <R:Any, T> scenario(symbolicObjectEvent: SymbolicObjectEvent<R, T>, triggeredScenario: suspend Scenario.(ObjectEvent<R, T>) -> Unit): suspend Scenario.() -> Unit = {
    while(true){
        waitFor(symbolicObjectEvent).launchScenario(triggeredScenario)
    }
}
fun <R:Any, T> scenario(objectEvent: ObjectEvent<R, T>, triggeredScenario: suspend Scenario.(ObjectEvent<R, T>) -> Unit): suspend Scenario.() -> Unit = {
    while(true){
        waitFor(objectEvent).launchScenario(triggeredScenario)
    }
}
fun scenario(initialingEvents : NonConcreteEventSet, triggeredScenario: suspend Scenario.(ObjectEvent<Any, *>) -> Unit): suspend Scenario.() -> Unit = {
    while(true){
        @Suppress("UNCHECKED_CAST")
        (waitFor(initialingEvents) as ObjectEvent<Any, *>).launchScenario(triggeredScenario)
    }
}

object RESUME


class ScenarioProgram(
    val name : String = "",
    val environmentMessageTypes : MutableSet<KFunction<ObjectEvent<*,*>>> = mutableSetOf(),
    val isControlledObject : (Any) -> Boolean = { _ : Any -> true },
    val isEnvironmentEvent : (ObjectEvent<*, *>) -> Boolean = { objectEvent -> environmentMessageTypes.containsKFunction(objectEvent.type) },
    val terminatingEvents : MutableSet<IEventSet> = mutableSetOf(),
    val activeAssumptionScenarios : MutableSet<suspend Scenario.() -> Unit> = hashSetOf(),
    val activeGuaranteeScenarios : MutableSet<suspend Scenario.() -> Unit> = hashSetOf(),
    eventNodeInputBufferSize : Int = 25,
    val superStepTerminationEvent : Event? = null,
    var terminateUponAssumptionViolation : Boolean = true,
    var terminateUponGuaranteeViolation : Boolean = false,
    var eventRecorder : MutableList<Event>? = null,
    val clock : Clock = Clock()
) {


    private val syncChannel: Channel<AbstractScenarioSyncMessage> = Channel()
    private val resumeChannel: Channel<Scenario> = Channel()
    private var numberOfScenariosAwaitedToReachSyncPoint = 0
    private val activeScenariosToLastScenarioSychMessage: MutableMap<Scenario, ScenarioSyncMessage> = mutableMapOf()
    private val suspendedScenarioToForbiddenEvents: MutableMap<Scenario, IEventSet> = mutableMapOf()
    private val scenariosInMustProgressSection : MutableSet<Scenario> = hashSetOf()

    private var terminatingEventOccurred = false

    var terminated : Boolean = false

    val guaranteeViolationExceptions = mutableListOf<ViolationException>()
    val assumptionViolationExceptions = mutableListOf<ViolationException>()

    var inActiveSuperStep = true

    private var canMakeNextStep : Boolean = true

    fun canMakeNextStep() = canMakeNextStep

    fun init() {
        // start active scenarios
        for (activeScenario in activeAssumptionScenarios){
            startNewScenario(ScenarioKind.ASSUMPTION, activeScenario)
        }
        for (activeScenario in activeGuaranteeScenarios){
            startNewScenario(ScenarioKind.GUARANTEE, activeScenario)
        }
    }

    /**
     * Executes a single step.
     *
     * Step procedure:
     *
     * 1.  Check whether suspended scenarios are ready tp be resumed. If so, resume them and return RESUMEDSCENARIOSEVENT.
     * 2.  Receive messages from all (non-suspended) scenarios
     * 3.  If there are no scenarios in a must-progress state and a terminating event has occurred, then terminate the scenario program.
     * 4.  Calculate selectable events
     * 5.  Select one of these events
     * 6.  If no event could be selected, terminate the scenario program
     * 7.  If the selected event is the (internal) event RESUMEDSCENARIOSEVENT, then return it.
     *     (This means that no event could be selected, but instead suspended scenarios were resumed and will possibly request events in the next step)
     * 8.  Invoke its side effects (i.e. "execute event")
     * 9.  Communicate the executed event via output channels to connected event nodes that registered to receive this event
     * 10. Notify all active scenarios that committed, requested, or waited-for this event
     *
     * If the event selection yielded No event
     *
     * Returned values: This method ...
     * a) returns an event if one was executed in this step, OR ELSE
     * b) returns the (internal) event RESUMEDSCENARIOSEVENT if a suspended scenario was resumed in this step, OR ELSE
     * c) returns TERMINATEDSCENARIOPROGRAMEVENT (if in the prev. step a terminating event has occurre or if the event selection did not yield an event to execute and there was no scenario to resume).
     */
    suspend fun makeStep(playOutEvent : Event = NOEVENT) : Event{

        assert(canMakeNextStep){"this method shall not be called when no next step is possible"}

        // Step 1
        if (!resumeChannel.isEmpty && resumeSuspendedScenarios())
            return RESUMEDSCENARIOSEVENT

        // Step 2
        receiveSynchMessagesFromScenarios()

        // Step 3
        if(scenariosInMustProgressSection.isEmpty() && terminatingEventOccurred){
            terminateScenarioProgram()
            return TERMINATEDSCENARIOPROGRAMEVENT
        }


        val selectedEvent = if (playOutEvent == NOEVENT) {
            // Step 4
            val selectableEvents = calculateSelectableEvents()
            // Step 5
            selectEvent(selectableEvents)
        }else {
            playOutEvent
        }

        when(selectedEvent){
            NOEVENT -> {
                // Step 6
                terminateScenarioProgram() // no event to select, not locally nor through connected event nodes: nothing more to do!
                return TERMINATEDSCENARIOPROGRAMEVENT
            }
            RESUMEDSCENARIOSEVENT -> {
                // Step 7
                return selectedEvent
            }
            else -> {
                // Step 8
                executeEvent(selectedEvent)

                if (!terminatingEventOccurred)
                    terminatingEvents.let {terminatingEventOccurred = it.contains(selectedEvent)}
                // Step 9
                eventNode.send(selectedEvent)
                // Step 10
                notifyActiveScenariosAfterEventSelection(selectedEvent)

                return selectedEvent
            }
        }
    }


    /**
     * Runs the scenario program.
     */
    suspend fun run() {
        init()

        while(canMakeNextStep){
            makeStep()
        }
    }

    /**
     * Runs the scenario program with a list of events.
     * Terminates when the list of events was executed, even
     * when the scenario program can continue execution.
     * Execution can be continues either by calling
     * continueRun() or by invoking makeStep().
     */
    suspend fun runEvents(eventsToPlayout : List<Event>) {
        logger.info("running scenario program and playing out an event list ${eventsToPlayout.subList(0, minOf(5, eventsToPlayout.size))} ...")
        clock.monitoringMode = true
        init()
        for(event in eventsToPlayout){
            makeStep(event)
        }
        clock.monitoringMode = false
        makeStep(clock.monitoringEnded())
    }

    /**
     * Continues the execution of a partially executed scenario program.
     */
    suspend fun continueRun()  {
        logger.info("continuing execution")
        while(canMakeNextStep){
            makeStep()
        }

    }

    private suspend fun terminateScenarioProgram() {
        fun printViolationOccurred() = if (violationOccurred()) "***VIOLATION OCCURRED!*** " else "NO VIOLATION OCCURRED. "
        canMakeNextStep = false
        notifyActiveScenariosAfterEventSelection(TERMINATEDSCENARIOPROGRAMEVENT)
        receiveSynchMessagesFromScenarios() // allow scenarios to do one last step, especially raise last violations upon termination.
        eventNode.terminate()
        logger.info { "Scenario program '$name' terminating. " +
                "${printViolationOccurred()} \n"+
                "Violation in guarantees: ${guaranteeViolationOccurred()} \n" +
                "Exceptions in guarantee scenarios: ${guaranteeViolationExceptions} \n" +
                "Guarantee scenarios left in must-progress-sections: ${guaranteeScenariosInMustProgressSection()} \n" +
                "Violation in assumptions: ${assumptionViolationOccurred()} \n" +
                "Exceptions in assumption scenarios:: ${assumptionViolationExceptions}  \n" +
                "Assumption scenarios left in must-progress-sections: ${assumptionScenariosInMustProgressSection()} " }
        terminated = true
    }


    //=====================================================================================
    // Starting scenarios
    //=====================================================================================

    private fun <R:Any, T>Scenario.runTriggeredScenario(initObjectEvent : ObjectEvent<R, T>, scenarioMain : suspend Scenario.(ObjectEvent<R, T>) -> Unit) = coroutineScope.launch {
        scenarioMain(initObjectEvent)
        terminate()
    }

    private fun Scenario.runTriggeredScenario(scenarioMain : suspend Scenario.() -> Unit) = coroutineScope.launch {
        scenarioMain()
        terminate()
    }

    private fun startNewScenario(scenarioKind: ScenarioKind, scenarioMain : suspend Scenario.() -> Unit){
        Scenario(syncChannel, resumeChannel, GlobalScope, scenarioKind, clock).runTriggeredScenario(scenarioMain)
        numberOfScenariosAwaitedToReachSyncPoint++
    }

    private fun <R:Any, T>startNewTriggeredScenario(addTriggeredScenarioMessage : AddTriggeredScenarioMessage<R,T>){
        Scenario(syncChannel, resumeChannel, GlobalScope, addTriggeredScenarioMessage.sender.scenarioKind, clock).runTriggeredScenario(addTriggeredScenarioMessage.initObjectEvent, addTriggeredScenarioMessage.scenarioMain)
        numberOfScenariosAwaitedToReachSyncPoint++
    }


    //=====================================================================================
    // Main event loop functions:
    //=====================================================================================


    /**
     * Step 2 in event selection procedure: receive synch messages from scenarios
     */
    private suspend fun receiveSynchMessagesFromScenarios() {
        while (numberOfScenariosAwaitedToReachSyncPoint > 0) {
            val abstractScenarioMessage = syncChannel.receive()
            when (abstractScenarioMessage) {
                is ScenarioSyncMessage -> {
                    activeScenariosToLastScenarioSychMessage.put(
                        abstractScenarioMessage.sender,
                        abstractScenarioMessage
                    )
                    if(abstractScenarioMessage.toggleMustProgressState) {
                        if (scenariosInMustProgressSection.contains(abstractScenarioMessage.sender)) {
                            scenariosInMustProgressSection.remove(abstractScenarioMessage.sender)
                        } else {
                            scenariosInMustProgressSection.add(abstractScenarioMessage.sender)
                        }
                    }
                    numberOfScenariosAwaitedToReachSyncPoint--
                }
                is SuspendMessage -> {
                    suspendedScenarioToForbiddenEvents.put(
                        abstractScenarioMessage.sender,
                        abstractScenarioMessage.forbiddenEvents
                    )
                    numberOfScenariosAwaitedToReachSyncPoint--
                }
                is TerminatingScenarioMessage -> {
                    if (suspendedScenarioToForbiddenEvents.containsKey(abstractScenarioMessage.sender))
                        suspendedScenarioToForbiddenEvents.remove(abstractScenarioMessage.sender)
                    else {
                        activeScenariosToLastScenarioSychMessage.remove(abstractScenarioMessage.sender)
                        numberOfScenariosAwaitedToReachSyncPoint--
                        abstractScenarioMessage.violationException?.let{
                            recordViolation(it, abstractScenarioMessage.sender.scenarioKind)
                            logger.error { name + ": " + abstractScenarioMessage.violationException }
                        }
                    }
                    scenariosInMustProgressSection.remove(abstractScenarioMessage.sender)
                }
                is AddActiveScenarioMessage ->
                    startNewScenario(
                        abstractScenarioMessage.sender.scenarioKind,
                        abstractScenarioMessage.scenarioMain)
                is AddTriggeredScenarioMessage<*, *> ->
                    startNewTriggeredScenario(abstractScenarioMessage)
            }
        }
    }


    /**
     * Step 3 in event selection procedure: calculate selectable events
     */
    private suspend fun calculateSelectableEvents(): IConcreteEventSet {

        val allAssumptionForbiddenEvents = MutableNonConcreteEventSet()
        val allGuaranteeForbiddenEvents = MutableNonConcreteEventSet()

        val allAssumptionCommittedEvents = MutableConcreteEventSet()
        val allGuaranteeCommittedEvents = MutableConcreteEventSet()

        val allAssumptionRequestedEvents = MutableConcreteEventSet()
        val allGuaranteeRequestedEvents = MutableConcreteEventSet()


        for (scenarioToEventsMapEntry in activeScenariosToLastScenarioSychMessage){
            if (scenarioToEventsMapEntry.key.scenarioKind == ScenarioKind.ASSUMPTION){
                allAssumptionForbiddenEvents.addEvents(scenarioToEventsMapEntry.value.forbiddenEvents)
                allAssumptionCommittedEvents.addEvents(scenarioToEventsMapEntry.value.committedEvents)
                allAssumptionRequestedEvents.addEvents(scenarioToEventsMapEntry.value.requestedEvents)
            } else {
                allGuaranteeForbiddenEvents.addEvents(scenarioToEventsMapEntry.value.forbiddenEvents)
                allGuaranteeCommittedEvents.addEvents(scenarioToEventsMapEntry.value.committedEvents)
                allGuaranteeRequestedEvents.addEvents(scenarioToEventsMapEntry.value.requestedEvents)
            }
        }

        val ENV : (Event) -> Boolean = { !allAssumptionForbiddenEvents.contains(it) && !isSystemEvent(it) }
        val SYS : (Event) -> Boolean = { !allGuaranteeForbiddenEvents.contains(it) && isSystemEvent(it) }

        if (allGuaranteeCommittedEvents.isNotEmpty()){
            val allSelectableEvents = allGuaranteeCommittedEvents.filter(SYS)
            if (allSelectableEvents.isEmpty()){
                for(e in allGuaranteeCommittedEvents) {
                    if (!isSystemEvent(e)) {
                        recordViolation("There are events committed by guarantee scenarios, but they are not SYSTEM EVENTS: $e.", ScenarioKind.GUARANTEE)
                    } else {
                        recordViolation("There are system events committed by guarantee scenarios, but all are forbidden by guarantee scenarios: $e.", ScenarioKind.GUARANTEE)
                    }
                }
            }
            else return ConcreteEventSet(allSelectableEvents.toSet())
        }

        if (allAssumptionCommittedEvents.isNotEmpty()){
            val allSelectableEvents = allAssumptionCommittedEvents.filter(ENV)
            if (allSelectableEvents.isEmpty()){
                for(e in allAssumptionCommittedEvents) {
                    if (isSystemEvent(e)) {
                        recordViolation("There are events committed by assumption scenarios, but they are not ENVIRONMENT EVENTS: $e.", ScenarioKind.ASSUMPTION)
                    } else {
                        recordViolation("There are environment events committed by assumption scenarios, but all are forbidden by assumption scenarios: $e.", ScenarioKind.ASSUMPTION)
                    }
                }
            }
            else return ConcreteEventSet(allSelectableEvents.toSet())
        }

        val allGuaranteeRequestedNonForbiddenSystemEvents = allGuaranteeRequestedEvents.filter(SYS)

        if (allGuaranteeRequestedNonForbiddenSystemEvents.isNotEmpty())
            return ConcreteEventSet(allGuaranteeRequestedNonForbiddenSystemEvents.toSet())
        else if (allGuaranteeRequestedEvents.isNotEmpty())
            logger.debug {"$name -- There are requested system events, but all are forbidden by guarantee scenarios: $allGuaranteeRequestedEvents. Commencing to select an environment event next..."}

        // If this part is reached, it means that the current super-step has ended.
        // If there are events declared to be executed on the termination of a super-step, then they are now selected.
        // The flag inActiveSuperStep is used to send the superStepTerminationEvent only once and then
        // commence the next super-step by waiting for the next external event to occur.
        if (superStepTerminationEvent != null
                && inActiveSuperStep){
            inActiveSuperStep = false
            return superStepTerminationEvent
        } else {
            inActiveSuperStep = true
        }

        // if there are connected scenarioPrograms sending us input, wait for first process the events from the input channel
        if (!eventNode.inputChannelIsEmpty()) eventNode.receive().let {
            if (eventNode.inputChannelIsFull())
                logger.info { "$name: Input channel with buffer size ${eventNode.inputChannelBufferSize} is full!" }
            logger.debug{"$name: Early processing of input channel event. Received: $it"}
            return it
        }


        val allAssumptionRequestedNonForbiddenEnvironmentEvents = allAssumptionRequestedEvents.filter(ENV)

        if (allAssumptionRequestedNonForbiddenEnvironmentEvents.isNotEmpty())
            return ConcreteEventSet(allAssumptionRequestedNonForbiddenEnvironmentEvents.toSet())

        if (resumeSuspendedScenarios())
            return RESUMEDSCENARIOSEVENT

        if(eventNode.sendersConnected())
            // if no local event can be selected, and there are no suspended scenarios, but there are connected scenarioPrograms sending us input, wait for the next event from the input channel
            eventNode.receive().let {
            logger.debug{"$name: LATE waiting for input channel event. Received: $it"}
            return it
        }

        if (allAssumptionRequestedEvents.isNotEmpty())
            for(e in allAssumptionRequestedEvents) {
                if (isSystemEvent(e)) {
                    recordViolation("There are events requested by assumption scenarios, but they are not ENVIRONMENT EVENTS: $e.", ScenarioKind.ASSUMPTION)
                }else{
                    recordViolation("There are environment events requested by assumption scenarios, but all are forbidden by assumption scenarios: $e.", ScenarioKind.ASSUMPTION)
                }
            }

        if (allGuaranteeRequestedEvents.isNotEmpty()){
                for(e in allGuaranteeRequestedEvents) {
                    if (!isSystemEvent(e)) {
                        recordViolation("There are events requested by guarantee scenarios, but they are not SYSTEM EVENTS: $e.", ScenarioKind.GUARANTEE)
                    }else{
                        recordViolation("There are system events requested by guarantee scenarios, but all are forbidden by guarantee scenarios: $e.", ScenarioKind.GUARANTEE)
                    }
                }
            }

        logger.debug {"$name -- No next event to select."}

        return NOEVENT
    }

    private fun MutableNonConcreteEventSet.addEvents(events : IEventSet) {
        if (!events.isNOEVENTS()) this.add(events)
    }

    private fun HashSet<Event>.addEvents(events : IConcreteEventSet) {
        if (!events.isNOEVENTS()) this.addAll(events)
    }

    //TODO: replace by recursive isEmpty() implementation for all IEventSet implementations.
    private fun IEventSet.isNOEVENTS() = (this.isEmpty() || this == NOEVENTS || this is Collection<*> && this.size == 1 && this.iterator().next() == NOEVENTS)

//    fun isSystemEvent(e : Event) = !(e is ObjectEvent<*,*>) || !(environmentMessageTypes.containsKFunction(e.type))
    fun isSystemEvent(e : Event) = e !is ObjectEvent<*,*> || !isEnvironmentEvent(e)


    /**
     * Step 4 in event selection procedure: select one selectable events
     */
    private fun selectEvent(selectableEvents: IConcreteEventSet): Event =
        // TODO Make extensible to plug in different event selection strategies
        if (selectableEvents.isNotEmpty())
            selectableEvents.random()
        else
            NOEVENT

    /**
     * Step 5 in event selection procedure: Invoke its side effects (i.e. "execute event")
     */
    private fun executeEvent(selectedEvent: Event) {
        logger.debug {"$name ########## ${if(clock.monitoringMode) "monitoring" else ""}${if(clock.time() != 0.0) "t=${clock.time()} " else ""} $name -- executing event: $selectedEvent" }

        if (selectedEvent is ObjectEvent<*,*>
            && isControlledObject(selectedEvent.receiver)){
            selectedEvent.callFunction()
            if (anyWaiting.contains(selectedEvent)){
                clock.increase(selectedEvent.parameters[0] as Double)
            }
        }

        eventRecorder?.add(selectedEvent)
    }

    /**
     * Step 7 in event selection procedure (after event selection and execution): notify all active scenarios that committed, requested, or waited-for this event
     */
    private suspend fun notifyActiveScenariosAfterEventSelection(selectedEvent: Event) {
        val scenarioToEventsMapEntryIterator = activeScenariosToLastScenarioSychMessage.iterator()

        while (scenarioToEventsMapEntryIterator.hasNext()) {
            val scenarioToEventsMapEntry = scenarioToEventsMapEntryIterator.next()
            if (scenarioToEventsMapEntry.value.waitedForEvents.contains(selectedEvent)
                || scenarioToEventsMapEntry.value.committedEvents.contains(selectedEvent)
                || scenarioToEventsMapEntry.value.requestedEvents.contains(selectedEvent)
                || scenarioToEventsMapEntry.value.forbiddenEvents.contains(selectedEvent)
                ) {
                scenarioToEventsMapEntry.key.notifyOfEventChannel.send(selectedEvent)
                scenarioToEventsMapEntryIterator.remove()
                numberOfScenariosAwaitedToReachSyncPoint++
            }
        }
    }


    /**
     * Called after each event-selection-and-execution step, try to resume suspended scenarios.
     */
    private suspend fun resumeSuspendedScenarios() : Boolean{
        return if(suspendedScenarioToForbiddenEvents.isNotEmpty()){
            do {
                val resumedScenario = resumeChannel.receive()
                numberOfScenariosAwaitedToReachSyncPoint++
                suspendedScenarioToForbiddenEvents.remove(resumedScenario)
            } while (!resumeChannel.isEmpty)
            true
        } else {
            false
        }
    }

    //=====================================================================================
    // Recording and throwing violations
    //=====================================================================================

    private fun recordViolation(message : String, scenarioKind: ScenarioKind) = recordViolation(ViolationException(message), scenarioKind)
    private fun recordViolation(violationException: ViolationException, scenarioKind: ScenarioKind) {
        logger.error { "$name: ${violationException.message}" }
        when (scenarioKind){
            ScenarioKind.GUARANTEE -> {
                guaranteeViolationExceptions.add(violationException)
                if (terminateUponGuaranteeViolation) throw CompositeViolationException()
            }
            ScenarioKind.ASSUMPTION -> {
                assumptionViolationExceptions.add(violationException)
                if (terminateUponAssumptionViolation) throw CompositeViolationException()
            }
        }
    }

    fun assumptionScenariosInMustProgressSection() = scenariosInMustProgressSection.filter { s -> s.scenarioKind == ScenarioKind.ASSUMPTION }
    fun guaranteeScenariosInMustProgressSection() = scenariosInMustProgressSection.filter { s -> s.scenarioKind == ScenarioKind.GUARANTEE }

    fun assumptionViolationOccurred() = assumptionViolationExceptions.isNotEmpty() || (terminated && assumptionScenariosInMustProgressSection().isNotEmpty())
    fun guaranteeViolationOccurred() = guaranteeViolationExceptions.isNotEmpty() || (terminated && guaranteeScenariosInMustProgressSection().isNotEmpty())
    fun violationOccurred() = assumptionViolationOccurred() || guaranteeViolationOccurred()
    fun specificationViolated() = !assumptionViolationOccurred() && guaranteeViolationOccurred()

    inner class CompositeViolationException() : RuntimeException() {
        fun getExceptions() = listOf(
            assumptionViolationExceptions,
            generateAssumptionMustProgressViolations(),
            guaranteeViolationExceptions,
            generateGuaranteeMustProgressViolations()).flatten()
        private fun generateAssumptionMustProgressViolations() = assumptionScenariosInMustProgressSection().map {
                e -> ViolationException("Assumption scenario name=\"${e.name}\" did not progress in a must-progress state.")}.toList()
        private fun generateGuaranteeMustProgressViolations() = guaranteeScenariosInMustProgressSection().map {
                e -> ViolationException("Guarantee scenario name=\"${e.name}\" did not progress in a must-progress state.")}.toList()

        override val message: String
            get() = when (violationOccurred()){
                true -> "\n" + assumptionViolationExceptions.joinToString(separator = "\n ") { it -> "ASSUMPTION VIOLATION: ${it.message}" } +
                        "\n ----- \n" + generateAssumptionMustProgressViolations().joinToString(separator = "\n ") { it -> "ASSUMPTION VIOLATION: ${it.message}" } +
                        "\n ----- \n" + guaranteeViolationExceptions.joinToString(separator = "\n ") { it -> "GUARANTEE VIOLATION: ${it.message}" } +
                        "\n ----- \n" + generateGuaranteeMustProgressViolations().joinToString(separator = "\n ") { it -> "ASSUMPTION VIOLATION: ${it.message}" }

                false -> ""
            }
    }

    //=====================================================================================
    // Functions for adding scenarios to the scenario program before running it. (To be done before running the scenario program)
    //=====================================================================================

    /**
     * Adds environment message types (KFunctions). To be done before running the scenario program.
     * Any event typed with a KFunction added here is called an "environment event", otherwise "system event".
     * - Only environment events can be committed/requested in assumption scenarios.
     * - Only system events can be committed/requested in guarantee scenarios.
     * - System events are selected with priority over environment events:
     *   1. Committed environment events are selected only when there are no committed system events
     *   2. Non-committed environment events are selected only when there are no system events committed or requested.
     */
    fun addEnvironmentMessageType(vararg kFunctions : KFunction<ObjectEvent<*,*>>){
        environmentMessageTypes.addAll(kFunctions)
    }

    //=============================================================================
    // Code for communicating with the scenario program / between scenario programs
    //=============================================================================

    val eventNode = EventNode(eventNodeInputBufferSize)

}

data class ViolationException(override val message : String = "", val scenarioName : String = "") : RuntimeException(message)

val TERMINATEDSCENARIOPROGRAMEVENT = NamedEvent("TerminatedScenarioProgramEvent")
val NOEVENT = NamedEvent("NOEVENT")
val RESUMEDSCENARIOSEVENT = NamedEvent("RESUMEDSCENARIOS")


class Clock() {

    private var time : Double = 0.0

    fun time() = time

    fun increase(waitingTime : Double) {
        time += waitingTime
    }

    fun wait(waitTime : Double, waitMessage : String = "") = event(waitTime, waitMessage){}

    override fun toString(): String {
        return "clock"
    }

    var monitoringMode = false

    fun monitoringEnded() = event(){}
}