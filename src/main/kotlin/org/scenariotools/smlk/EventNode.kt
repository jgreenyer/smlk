package org.scenariotools.smlk

import kotlinx.coroutines.channels.Channel

class EventNode(val inputChannelBufferSize : Int = 0) {

    private val inputChannel = Channel<Event>(inputChannelBufferSize)
    private val outputChannelsToSubscribedEventSet = mutableMapOf<Channel<Event>,IEventSet>()
    private val senders = mutableSetOf<EventNode>()
    private val receivers = mutableSetOf<EventNode>()

    private var terminated = false

    fun registerSender(senderEventNode : EventNode, subscribeToEvents : IEventSet){
        senders.add(senderEventNode)
        senderEventNode.receivers.add(this)
        senderEventNode.outputChannelsToSubscribedEventSet.put(inputChannel, subscribeToEvents)
    }
    suspend fun unregisterSender(senderEventNode : EventNode){
        senders.remove(senderEventNode)
        senderEventNode.receivers.remove(this)
        senderEventNode.outputChannelsToSubscribedEventSet.remove(inputChannel)

        if (!isTerminated() && senders.isEmpty()) {
            inputChannel.send(AllSendersUnregisteredEvent)
        }
    }
    suspend fun terminate() {
        terminated = true
        receivers.forEach {  it.unregisterSender(this) }
    }

    fun isTerminated() = terminated

    fun inputChannelIsEmpty() = inputChannel.isEmpty
    fun inputChannelIsFull() = inputChannel.isFull

    fun sendersConnected() = !senders.isEmpty()
    fun senders() = senders as Set<EventNode>

    suspend fun receive() = inputChannel.receive()

    /**
     * Step 5 in event selection procedure: communicate executed event via output channels to connected event nodes that registered to receive this event
     */
    suspend fun send(event: Event) {
        outputChannelsToSubscribedEventSet.forEach{
            if (it.value.contains(event)){
                it.key.send(event)
            }
        }
    }
}

object AllSendersUnregisteredEvent: Event() {
    override fun toString() = "AllSendersUnregisteredEvent"
}