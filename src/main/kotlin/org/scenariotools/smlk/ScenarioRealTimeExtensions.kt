package org.scenariotools.smlk.examples.symbolicrealtime

import mu.KotlinLogging
import org.scenariotools.smlk.*
import java.util.*
import kotlin.math.ln
import kotlin.math.nextDown
import kotlin.math.nextUp


private val logger = KotlinLogging.logger {}

val anyWaiting = Clock::wait.symbolicEvent().param(ClosedDoubleRange(0.0.nextUp(), Double.POSITIVE_INFINITY))

val monitoringEndedEvent = Clock::monitoringEnded.symbolicEvent()

val random = Random()

/**
 * wait the waitingTime and then request one of the requestEvents
 * without allowing further waiting.
 * The forbiddenEvents are forbidden during the waiting time until after
 * one of the requestEvents are executed.
 *
 * If one of the requestEvents occurs before the end of the waiting
 * time, then terminate, i.e., abort waiting and do not request any
 * of the request events
 *
 * Returns:
 * - the instance of the requestedEvents that was execucted
 *   as a result of requesting it at the end of the waiting time
 * - or the instance of the requestEvent that occurred before the
 *   waiting time was over.
 * - or the NOEVENT Event if there was
 */
suspend fun Scenario.wait(waitingTime: Double, requestEvents: IConcreteEventSet, forbiddenEvents : IEventSet = NOEVENTS, waitMessage : String) : Event{
    val t0 = clock.time()
    fun remainingTime() = if (clock.monitoringMode) Double.POSITIVE_INFINITY else (t0 + waitingTime) - clock.time()
    var event : Event?
    while (remainingTime() > 0){
        event = request(
            // request waiting the remaining amount of time
            requestEvents = if (clock.monitoringMode) NOEVENT else clock.wait(remainingTime(), waitMessage),
            // progress when requested event occurs early or more time passes
            // (when more time passes, we have to re-compute the remaining
            //  waiting time and re-request waiting that amount of time.)
            // also when the monitoring mode ended, progress and re-compute the
            // remaining time to wait.
            waitedForEvents = requestEvents union anyWaiting union monitoringEndedEvent,
            // no wait event is allowed that waits longer than this one
            forbidden = if (clock.monitoringMode) forbiddenEvents
                    else forbiddenEvents union Clock::wait.symbolicEvent().param(
                ClosedDoubleRange(remainingTime().nextUp(), Double.POSITIVE_INFINITY))
        )
        if (requestEvents.contains(event)) {
            return event
        }
    }
    return if (requestEvents == NOEVENT) {
        NOEVENT
    }else{
        logger.debug("waiting time is over, now urgently requesting $requestEvents")
        request(requestEvents, forbidden = forbiddenEvents union anyWaiting)
    }
}

/**
 * Wait the waitingTime.
 * The forbiddenEvents are forbidden during the waiting time
 */
suspend fun Scenario.wait(waitingTime: Double, forbiddenEvents : IEventSet = NOEVENTS, waitMessage : String){
    val t0 = clock.time()
    fun remainingTime() = (t0 + waitingTime) - clock.time()
    while (remainingTime() > 0){
        request(
            // request waiting the remaining amount of time
            requestEvents = clock.wait(remainingTime(), waitMessage),
            // progress when more time passes
            // (when more time passes, we have to re-compute the remaining
            //  waiting time and re-request waiting that amount of time.)
            waitedForEvents = anyWaiting,
            // no wait event is allowed that waits longer than this one
            forbidden = forbiddenEvents union Clock::wait.symbolicEvent().param(
                ClosedDoubleRange(remainingTime().nextUp(), Double.POSITIVE_INFINITY))
        )
    }
}

suspend fun Scenario.urgent(subScenario: suspend Scenario.() -> Unit){
    subScenario before anyWaiting
}


/**
 * lambda = Number of expected events per time unit, see https://stackoverflow.com/questions/29020652/java-exponential-distribution
 */
fun Random.nextDDoubleWithExpDistribution(lambda : Double) = ln(1-random.nextDouble()) /(-lambda)

suspend fun Scenario.requestAfterRandomTimeWithExpDistribution(
        meanWaitingTime: Double = 0.0,
        requestEvents: IConcreteEventSet,
        forbiddenEvents : IEventSet = NOEVENTS,
        waitMessage : String) = wait(
            random.nextDDoubleWithExpDistribution(1 / meanWaitingTime),  // lambda = 1 / E[X]
            requestEvents = requestEvents,
            forbiddenEvents = forbiddenEvents, waitMessage)



