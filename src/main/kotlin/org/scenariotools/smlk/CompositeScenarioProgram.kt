package org.scenariotools.smlk

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


class CompositeScenarioProgram{

    private val scenarioPrograms : MutableSet<ScenarioProgram> = mutableSetOf()

    /**
     * Adds a scenario program to the composite scenario program.
     */
    fun addScenarioProgram(vararg scenarioProgram: ScenarioProgram){
        scenarioPrograms.addAll(scenarioProgram)
    }

    /**
     * Runs all scenario programs in the composite scenario program.
     */
    fun run() {
        runBlocking {
            for (scenarioProgram in scenarioPrograms){
                launch { scenarioProgram.run() }
            }
        }
    }

}