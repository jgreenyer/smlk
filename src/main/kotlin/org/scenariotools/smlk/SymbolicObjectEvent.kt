package org.scenariotools.smlk

import java.lang.IllegalArgumentException
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.full.valueParameters


class SymbolicObjectEvent<R:Any,T>(val receiver:R? = null, val type: KFunction<ObjectEvent<R,T>>, vararg val parameters : Any, val sender : Any? = null) :
    IEventSet {

    /**
     * Checks whether the given element is in the set of object events represented by this symbolic object event.
     *
     * The method returns true if
     * (1) the signature of the object event is compatible to that of the symbolic object event. This is the case when
     *     (a) the type of the object event is equal
     *     (b) the receiver specified for this symbolic object event is null,
     *         or a class of which the receiver of the object event is an instance,
     *         or the same object as the receiver of the object event.
     * (2) the parameters of the object event are compatible to those of the symbolic object event.
     *     If this symbolic message has i parameters and the compared object event has j parameters,
     *     then if i == j, each object event parameter must be compatible scenario the symbolic event parameter counterpart.
     *     The object event parameter is compatible to a symbolic object event parameter if a concrete parameter value
     *     is specified by the symbolic object event, and the values are equal, if a set is specified by the symbolic
     *     object event, e.g., an integer range, and the object event parameter is an element of that set,
     *     or if the symbolic object event specifies the ANY value.
     *     If i < j, then the first i object event parameters must be unifiable to the i symbolic object event
     *     parameters, in their order, and all remaining object event parameters are ignored, i.e., it is equivalent to
     *     checking them against the ANY value.
     *     If i > j, the rationale is that the symbolic object event specifically requires more parameters, and,
     *     therefore, we return false in this case.
     */
    override fun contains(element: Event): Boolean {
        if (element !is ObjectEvent<*,*>) {
            return false
        }
        if (!signatureUnifyable(element)) {
            return false
        }
        if (!senderMatches(element)){
            return false
        }
        if (parameters.size > element.parameters.size) return false
        for (i in parameters.indices){
            val thisParamVal = parameters[i]


            val otherParamVal = element.parameters[i]
            if (thisParamVal == otherParamVal)
                continue
            when (thisParamVal){
                ANY -> {}
                is Collection<*> -> if (!thisParamVal.contains(otherParamVal)) return false
                is IntRange ->
                    if (otherParamVal !is Int || !thisParamVal.contains(otherParamVal)) {
                        return false
                    }
                is LongRange ->
                    if (otherParamVal !is Long || !thisParamVal.contains(otherParamVal)) {
                        return false
                    }
                is ClosedDoubleRange ->{
                    if (otherParamVal !is Double || !thisParamVal.contains(otherParamVal)){
                        return false
                    }

                }
                else -> {
                        //parameter values not compatible
                        return false
                }
            }
        }
        return true
    }

    private fun signatureUnifyable(objectEvent: ObjectEvent<*,*>): Boolean {

        if (!type.kFunctionEquals(objectEvent.type))
            return false

        if (!unifiable(receiver, objectEvent.receiver))
            return false

        return true
    }

    private fun senderMatches(objectEvent: ObjectEvent<*,*>) =
        when (sender) {
            // if sender == null it means there need not be, but there may be a sender
            null -> true
            // if sender == ANY it means that there must any non-null sender
            ANY -> objectEvent.sender != null
            else -> if (objectEvent.sender != null)
                    unifiable(sender, objectEvent.sender)
                else false
        }

    override fun isEmpty() = false

    override fun toString(): String {
        return receiver.toString() + "." + type.name + parameters.joinToString(separator = ", ", prefix = "(", postfix = ")")
    }

}

private fun unifiable(symbolicObj : Any?, obj : Any) : Boolean {
    if (symbolicObj == null || symbolicObj == ANY)
        return true
    else if (symbolicObj is KClass<*>){ // obj unbound, then types must be unifiable
        return (symbolicObj.isInstance(obj))
    } else { // obj bound, then bound objects must be equal
        return  (symbolicObj == obj)
    }
}


class UntypedSymbolicObjectEvent<R:Any>(val receiver:R? = null) :
    IEventSet {

    override fun contains(element: Event): Boolean {
        if (!(element is ObjectEvent<*,*>))
            return false
        else if (!unifiable(receiver, element.receiver))
            return false

        return true
    }

    override fun isEmpty() = false

    override fun toString(): String {
        return receiver.toString() + ".<ANY>"
    }

}

object ANY{
    override fun toString() = "ANY"
}



fun <R:Any,T> KFunction<ObjectEvent<R,T>>.symbolicEvent() = SymbolicObjectEvent<R,T>(type = this)
fun <R:Any, T> KFunction<ObjectEvent<R,T>>.symbolicEvent(vararg parameters : Any) = SymbolicObjectEvent<R,T>(type = this, parameters = parameters)

infix fun <R:Any,T> R.receives(kFunction : KFunction<ObjectEvent<R,T>>) = SymbolicObjectEvent<R,T>(this,kFunction)

fun <R:Any> R.receivesANY() = UntypedSymbolicObjectEvent<R>(this)

//infix fun <R:Any,T> SymbolicObjectEvent<R,T>.param(p : Any) = SymbolicObjectEvent<R,T>(receiver, type, *parameters, p)
@JvmName("paramIndex")
infix fun <R:Any,T> SymbolicObjectEvent<R,T>.param(p : Pair<Int,Any>) = this.param(p.first, p.second)
@JvmName("paramString")
infix fun <R:Any,T> SymbolicObjectEvent<R,T>.param(p : Pair<String,Any>) = this.param(p.first, p.second)


infix fun <R:Any,T> SymbolicObjectEvent<R,T>.param(params : List<Any>) =
    SymbolicObjectEvent(this.receiver, this.type, parameters = params.toTypedArray(), sender = this.sender)

fun <R:Any,T> SymbolicObjectEvent<R,T>.param(vararg params : Any) =
    SymbolicObjectEvent(this.receiver, this.type, parameters = params, sender = this.sender)

fun <R:Any,T> SymbolicObjectEvent<R,T>.param(index : Int, p : Any) : SymbolicObjectEvent<R,T> {
    val parametersSizeCurrent = parameters.size
    val parametersSizeNew = if (index+1 > parametersSizeCurrent) index+1 else parameters.size
    val newParameters = Array<Any>(parametersSizeNew){ _ -> ANY}
    for (i in parameters.indices) newParameters[i] = parameters[i]
    newParameters[index] = p
    return SymbolicObjectEvent<R,T>(receiver, type, parameters = newParameters, sender=this.sender)
}
fun <R:Any,T> SymbolicObjectEvent<R,T>.param(parameterName : String, p : Any) : SymbolicObjectEvent<R,T> {
    for (i in type.valueParameters.indices){
        if (type.valueParameters[i].name == parameterName){
            return param(i, p)
        }
    }
    throw IllegalArgumentException("No parameter with name '$parameterName' exists for KFunction ${type.name}. Only parameters: ${type.valueParameters.joinToString{"${it.name}"}}")
}


fun Set<KFunction<ObjectEvent<*,*>>>.symbolicEvents() : IEventSet {
    val mutableNonConcreteEventSet = MutableNonConcreteEventSet()
    @Suppress("UNCHECKED_CAST")
    this.forEach { mutableNonConcreteEventSet.add((it as KFunction<ObjectEvent<Any,Any?>>).symbolicEvent())}
    return mutableNonConcreteEventSet
}

infix fun <R:Any, T> Any.sends(symbolicObjectEvent : SymbolicObjectEvent<R, T>) : SymbolicObjectEvent<R, T> =
    SymbolicObjectEvent(symbolicObjectEvent.receiver, symbolicObjectEvent.type, parameters = symbolicObjectEvent.parameters, sender = this)

infix fun <A:Any, B:Any> A.sends(receiver : B) : Pair<A, B> = this to receiver
infix fun Pair<Any, Any>.receives(receiver : Any) : Pair<Any, Any> = this to receiver
infix fun <R:Any,T> Pair<Any,R>.receives(kFunction : KFunction<ObjectEvent<R,T>>) = SymbolicObjectEvent<R,T>(this.second,kFunction, sender = this.first)

