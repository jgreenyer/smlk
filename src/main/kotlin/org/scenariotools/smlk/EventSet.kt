package org.scenariotools.smlk

import java.lang.UnsupportedOperationException
import kotlin.collections.HashSet

interface IEventSet{
    fun contains (element : Event) : Boolean
    fun isEmpty() : Boolean
}

interface IConcreteEventSet : IEventSet, Collection<Event> {}

open class MutableNonConcreteEventSet(vararg events : IEventSet) : IEventSet, HashSet<IEventSet>(events.asList()){
    override fun contains(element: Event) = this.any { eventSet -> eventSet.contains(element)}
}

open class NonConcreteEventSet(val events : Set<IEventSet>, val kind : String  = "") : IEventSet, Set<IEventSet> by events{
    constructor(vararg events : IEventSet, kind : String = "") : this (events.toSet(), kind)
    override fun contains(element: Event) = this.any { eventSet -> eventSet.contains(element)}
    override fun toString() = "NonConcreteEventSet($kind): $events"
}

open class NonConcreteEventSetExcludingEvents(val baseEventSet : IEventSet, val excludingEventSet : IEventSet) : IEventSet {
    override fun contains(element: Event) =
        !excludingEventSet.contains(element) && baseEventSet.contains(element)
    override fun isEmpty() = baseEventSet.isEmpty()
    override fun toString() = "NonConcreteEventSetExcludingEvents: baseEventSet=$baseEventSet -- excludingEventSet=$excludingEventSet"
}

open class ConcreteEventSet(events: Set<Event>) : IConcreteEventSet, Set<Event> by events{
    constructor(vararg event : Event) : this(event.toSet())
    override fun toString() = "ConcreteEventSet: ${super.toString()}"
}

open class MutableConcreteEventSet : HashSet<Event>,
    IConcreteEventSet {
    constructor() : super()
    constructor(c: MutableCollection<Event>) : super(c)
    constructor(vararg event : Event) : super(event.toMutableSet())
    override fun toString() = "MutableConcreteEventSet: ${super.toString()}"
}

open class ComplementEventSet(val events : IEventSet) : IEventSet{
    override fun contains(element: Event): Boolean {
        return !events.contains(element)
    }
    override fun isEmpty() = false
    override fun toString() = "ComplementEventSet: + ${this.events}"
}

object ALLEVENTS : IEventSet {
    override fun contains(element: Event) = true

    override fun isEmpty() = false

    override fun toString() = "ALLEVENTS"
}


object NOEVENTS : IConcreteEventSet {
    override val size: Int
        get() = 0

    override fun containsAll(elements: Collection<Event>) = false

    override fun isEmpty() = true

    override fun iterator(): Iterator<Event> =
        EmptyEventIterator()

    override fun contains(element: Event) = false

    private class EmptyEventIterator() : Iterator<Event>{

        override fun hasNext() = false

        override fun next(): Event {
            throw UnsupportedOperationException()
        }

    }

    override fun toString() = "NOEVENTS"
}

infix fun IEventSet.union(events : IEventSet) = NonConcreteEventSet(this, events, kind = "UNION")
infix fun ConcreteEventSet.union(event : Event) = ConcreteEventSet(*this.toTypedArray(), event)
infix fun IEventSet.excluding(events : IEventSet) = NonConcreteEventSetExcludingEvents(this, events)
fun complement(events : IEventSet) = ComplementEventSet(events)