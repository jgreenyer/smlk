package org.scenariotools.smlk

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import java.util.*
import kotlin.reflect.KFunction

enum class ScenarioKind {
    ASSUMPTION, GUARANTEE
}

class Scenario(
    private val syncChannel: Channel<AbstractScenarioSyncMessage>,
    private val resumeChannel: Channel<Scenario>,
    val coroutineScope: CoroutineScope,
    val scenarioKind: ScenarioKind,
    val clock : Clock) {

    val notifyOfEventChannel : Channel<Event> = Channel()

    var name = ""

    private var toggleMustProgress = false

    private var suspended = false

    companion object {
        val random = Random()
    }

    suspend fun terminate(cause : String? = null, violationException: ViolationException? = null){
        if (suspended)
            resume()
        syncChannel.send(TerminatingScenarioMessage(this, violationException))
        throw CancellationException(cause)
    }

    suspend fun resume() {
        assert(!suspended) {"Scenario is not suspended, thus cannot be resumed."}
        resumeChannel.send(this)
        suspended = false
    }

    suspend fun suspend(forbiddenEvents: IEventSet = NOEVENTS) {
        syncChannel.send(SuspendMessage(this, forbiddenEvents))
    }

    suspend fun suspendForMillis(timeMillis: Long, forbiddenEvents: IEventSet = NOEVENTS){
        this.suspend(forbiddenEvents)
        delay(timeMillis)
        this.resume()
    }

    suspend fun suspendForTask(forbiddenEvents: IEventSet = NOEVENTS, longRunningTask : () -> Unit) {
        suspend(forbiddenEvents)
        longRunningTask.invoke()
        resume()
    }

    suspend fun sync(
        waitedForEvents : IEventSet = NOEVENTS,
        committedEvents: IConcreteEventSet = NOEVENTS,
        requestedEvents: IConcreteEventSet = NOEVENTS,
        forbiddenEvents : IEventSet = NOEVENTS) : Event {
        val syncMessage = ScenarioSyncMessage(this, waitedForEvents, committedEvents, requestedEvents, forbiddenEvents, toggleMustProgress)
        syncChannel.send(syncMessage)
        toggleMustProgress = false
        val receivedEvent = notifyOfEventChannel.receive()
        if(forbiddenEvents.contains(receivedEvent))
            violation("Event $receivedEvent is forbidden by scenario \"$name\", forbidden events = $forbiddenEvents")
        if ((interruptingEvents).contains(receivedEvent)){
            terminate()
        }
        return receivedEvent
    }

    val interruptingEvents = MutableNonConcreteEventSet()
    val forbiddenEvents = MutableNonConcreteEventSet()

    suspend fun waitFor(waitedForEvents : IEventSet, forbidden : IEventSet = NOEVENTS) : Event {
        return sync(waitedForEvents = MutableNonConcreteEventSet(*interruptingEvents.toTypedArray(), waitedForEvents), forbiddenEvents = forbiddenEvents union forbidden)
    }
    suspend fun waitForOverrideLocalForbidden(waitedForEvents : IEventSet, forbidden : IEventSet = NOEVENTS) : Event {
        return sync(waitedForEvents = MutableNonConcreteEventSet(*interruptingEvents.toTypedArray(), waitedForEvents), forbiddenEvents = (forbiddenEvents union forbidden) excluding waitedForEvents)
    }

    suspend fun commit(committedEvents: IConcreteEventSet, forbidden : IEventSet = NOEVENTS) : Event {
        return sync(waitedForEvents = interruptingEvents, committedEvents = committedEvents, forbiddenEvents = forbiddenEvents union forbidden)
    }
    suspend fun commitOverrideLocalForbidden(committedEvents: IConcreteEventSet, forbidden : IEventSet = NOEVENTS) : Event {
        return sync(waitedForEvents = interruptingEvents, committedEvents = committedEvents, forbiddenEvents = (forbiddenEvents union forbidden) excluding committedEvents)
    }

    suspend fun request(requestEvents: IConcreteEventSet, waitedForEvents: IEventSet = NOEVENTS, forbidden : IEventSet = NOEVENTS) : Event {
        return sync(waitedForEvents = waitedForEvents union interruptingEvents, requestedEvents = requestEvents, forbiddenEvents = forbiddenEvents union forbidden)
    }
    suspend fun requestOverrideLocalForbidden(requestEvents: IConcreteEventSet, waitedForEvents: IEventSet = NOEVENTS, forbidden : IEventSet = NOEVENTS) : Event {
        return sync(waitedForEvents = waitedForEvents union interruptingEvents, requestedEvents = requestEvents, forbiddenEvents = (forbiddenEvents union forbidden) excluding requestEvents)
    }


    @Suppress("UNCHECKED_CAST")
    suspend fun <R:Any,T> waitFor(objectEvent: ObjectEvent<R,T>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> = waitFor(objectEvent as IEventSet, forbidden = forbidden) as ObjectEvent<R,T>
    @Suppress("UNCHECKED_CAST")
    suspend fun <R:Any,T> waitForOverrideLocalForbidden(objectEvent: ObjectEvent<R,T>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> = waitForOverrideLocalForbidden(objectEvent as IEventSet, forbidden = forbidden) as ObjectEvent<R,T>
    @Suppress("UNCHECKED_CAST")
    suspend fun <R:Any,T> waitFor(symbolicObjectEvent: SymbolicObjectEvent<R,T>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> = waitFor(symbolicObjectEvent as IEventSet, forbidden = forbidden) as ObjectEvent<R,T>
    @Suppress("UNCHECKED_CAST")
    suspend fun <R:Any,T> waitFor(kFunction: KFunction<ObjectEvent<R,T>>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> = waitFor(SymbolicObjectEvent(type = kFunction) as IEventSet, forbidden = forbidden) as ObjectEvent<R,T>
    @Suppress("UNCHECKED_CAST")
    suspend fun <R:Any,T> waitForOverrideLocalForbidden(symbolicObjectEvent: SymbolicObjectEvent<R,T>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> = waitForOverrideLocalForbidden(symbolicObjectEvent as IEventSet, forbidden = forbidden) as ObjectEvent<R,T>
    @Suppress("UNCHECKED_CAST")
    suspend fun <R:Any,T> commit(objectEvent: ObjectEvent<R,T>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> = commit(objectEvent as IConcreteEventSet, forbidden = forbidden) as ObjectEvent<R,T>
    @Suppress("UNCHECKED_CAST")
    suspend fun <R:Any,T> commitOverrideLocalForbidden(objectEvent: ObjectEvent<R,T>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> = commitOverrideLocalForbidden(objectEvent as IConcreteEventSet, forbidden = forbidden) as ObjectEvent<R,T>
    @Suppress("UNCHECKED_CAST")
    suspend fun <R:Any,T> request(objectEvent: ObjectEvent<R,T>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> = request(objectEvent as IConcreteEventSet, forbidden = forbidden) as ObjectEvent<R,T>
    @Suppress("UNCHECKED_CAST")
    suspend fun <R:Any,T> requestOverrideLocalForbidden(objectEvent: ObjectEvent<R,T>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> = requestOverrideLocalForbidden(objectEvent as IConcreteEventSet, forbidden = forbidden) as ObjectEvent<R,T>

    @Suppress("UNCHECKED_CAST")
    suspend fun <R:Any,T> requestParamValuesMightVary(objectEvent: ObjectEvent<R, T>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> {
        val acceptableEvents = objectEvent.type.symbolicEvent()
        return sync(waitedForEvents = interruptingEvents union acceptableEvents, requestedEvents = objectEvent, forbiddenEvents = forbiddenEvents union forbidden) as ObjectEvent<R,T>
    }

    @Suppress("UNCHECKED_CAST")
    suspend fun <R:Any,T> requestParamValuesExact(objectEvent: ObjectEvent<R, T>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> {
        val inAcceptableEvents = objectEvent.type.symbolicEvent()
        return sync(waitedForEvents = interruptingEvents, requestedEvents = objectEvent, forbiddenEvents = forbiddenEvents union forbidden union inAcceptableEvents excluding objectEvent)  as ObjectEvent<R,T>
    }

    suspend fun violation(msg:String="") = terminate(msg, ViolationException(msg, name))

    suspend fun invoke(scenarioMain: suspend Scenario.() -> Unit) = scenarioMain.invoke(this)

    suspend fun launchScenario(scenarioMain: suspend Scenario.() -> Unit) = syncChannel.send(AddActiveScenarioMessage(this, scenarioMain))
    suspend fun <R:Any,T> ObjectEvent<R,T>.launchScenario(scenarioMain: suspend Scenario.(ObjectEvent<R,T>) -> Unit) = syncChannel.send(AddTriggeredScenarioMessage(this@Scenario, this, scenarioMain))

    suspend fun unordered(waitedForEvents: Set<IEventSet> = setOf(), requestedEvents: Set<Event> = setOf(), forbiddenEvents: IEventSet = NOEVENT) : List<Event>{
        val observedEvents = mutableListOf<Event>()
        val toBeWaitedForEvents = waitedForEvents.toMutableSet()
        val toBeRequestedEvents = requestedEvents.toMutableSet()
        while(toBeWaitedForEvents.isNotEmpty() || toBeRequestedEvents.isNotEmpty()){
            val executedEvent =  sync(
                    waitedForEvents = NonConcreteEventSet(toBeWaitedForEvents),
                    requestedEvents = ConcreteEventSet(toBeRequestedEvents),
                    forbiddenEvents = forbiddenEvents)
            if (!toBeWaitedForEvents.removeIf{ it.contains(executedEvent) })
                toBeRequestedEvents.removeIf{ it.contains(executedEvent) }
            observedEvents.add(executedEvent)
        }
        return observedEvents
    }

    suspend fun mustProgress(mustProgressFragment : suspend Scenario.() -> Unit) {
        toggleMustProgress = !toggleMustProgress // toggleMustProgress = true unless a mustProgress {...} section is followed immediately by another.
        mustProgressFragment.invoke(this)
        toggleMustProgress = true
    }

    suspend fun <R:Any,T> mustOccur(symbolicObjectEvent: SymbolicObjectEvent<R,T>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> {
        toggleMustProgress = !toggleMustProgress
        val event = waitFor(symbolicObjectEvent, forbidden)
        toggleMustProgress = true
        return event
    }

    suspend fun mustOccur(events: IEventSet, forbidden : IEventSet = NOEVENTS) : Event {
        toggleMustProgress = !toggleMustProgress
        val event = waitFor(events, forbidden)
        toggleMustProgress = true
        return event
    }

    suspend fun <R:Any,T> mustOccur(objectEvent: ObjectEvent<R,T>, forbidden : IEventSet = NOEVENTS) : ObjectEvent<R,T> {
        toggleMustProgress = !toggleMustProgress
        val event = waitFor(objectEvent, forbidden)
        toggleMustProgress = true
        return event
    }

    suspend fun mustOccur(condition : () -> Boolean) {
        toggleMustProgress = !toggleMustProgress
        waitFor(condition)
        toggleMustProgress = true
    }

    suspend infix fun IEventSet.interrupts(scenarioFragment : suspend Scenario.() -> Unit) {
        this@Scenario.interruptingEvents.add(this)
        scenarioFragment.invoke(this@Scenario)
        this@Scenario.interruptingEvents.remove(this)
    }
    suspend infix fun IEventSet.forbiddenIn(scenarioFragment : suspend Scenario.() -> Unit) {
        this@Scenario.forbiddenEvents.add(this)
        scenarioFragment.invoke(this@Scenario)
        this@Scenario.forbiddenEvents.remove(this)
    }

    suspend infix fun (suspend Scenario.() -> Unit).before(forbiddenEvents : IEventSet) {
        this@Scenario.forbiddenEvents.add(forbiddenEvents)
        this.invoke(this@Scenario)
        this@Scenario.forbiddenEvents.remove(forbiddenEvents)
    }

    suspend fun waitFor (condition : () -> Boolean) {
        while(!condition.invoke()) {
            waitFor(ALLEVENTS)
        }
    }

    suspend fun withProbability(probability:Double, behavior : suspend Scenario.() -> Unit){
        if(probability >= random.nextDouble()){ // probability that waiting happens
            invoke(behavior)
        }
    }


    override fun toString() : String {
        if (name != "") return name
        else return super.toString()
    }

}

