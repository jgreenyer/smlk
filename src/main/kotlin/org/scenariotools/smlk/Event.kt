package org.scenariotools.smlk

abstract class Event() : IConcreteEventSet {
    override val size: Int
        get() = 1

    override fun containsAll(elements: Collection<Event>) =
        if (elements.isEmpty()) {
            true
        } else if (elements.size == 1) {
            contains(elements.first())
        } else {
            false
        }

    override fun isEmpty(): Boolean {
        return false
    }

    override fun iterator(): Iterator<Event> {
        return SingleEventIterator(this)
    }

    override fun contains(element: Event): Boolean {
        return this == element
    }

    private class SingleEventIterator(private val e : Event) : Iterator<Event>{

        var hasNext = true

        override fun hasNext(): Boolean {
            return hasNext
        }

        override fun next(): Event {
            hasNext = false
            return e
        }

    }
}

class NamedEvent(val name : String) : Event() {
    override fun contains(element: Event) = when(element) {
        is NamedEvent -> element.name == name
        else -> false
    }

    override fun toString(): String {
        return "Named Event : \"$name\""
    }
}
