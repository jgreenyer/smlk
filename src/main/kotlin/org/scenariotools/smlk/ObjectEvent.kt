package org.scenariotools.smlk

import mu.KotlinLogging
import kotlin.reflect.KFunction

private val logger = KotlinLogging.logger {}

interface IFunctionCallEvent{
    fun callFunction()
}

interface IFunctionCallWithResultEvent : IFunctionCallEvent {
    fun result() : Any?
}

class ObjectEvent<R:Any,T>(val receiver:R, val type:KFunction<ObjectEvent<R,T>>, vararg val parameters : Any, val sender:Any? = null, val sideEffect: () -> T) : Event(),
    IFunctionCallWithResultEvent {

    override fun hashCode() =
            (31 * receiver.hashCode()
                    + 31 * type.hashCode()
                    + parameters.sumBy { 31 * it.hashCode() })

    override fun equals(other : Any?): Boolean {
        if (other is ObjectEvent<*,*>)
            return other.signatureEquals(receiver, type)
                    && other.sender == sender
                    && parameterValuesEqual(other)
        else
            return false
    }


    override fun toString(): String {
        fun functionCallResultString() = if (functionCallResult != null && !(functionCallResult is Unit)) " = " + functionCallResult.toString() else ""
        val senderString = if(sender != null){"${sender.toString()}->"} else {""}
        return "$senderString$receiver.${type.name}${parameterString()}" + functionCallResultString()
    }

    fun parameterString(separator :String = ", ") =
            parameters.filterIndexed { i, param -> i in 0 until type.parameters.size-1 }
                    .mapIndexed { i, param -> "${type.parameters[i+1].name} = $param"}
                    .joinToString(separator = separator, prefix = "(", postfix = ")")


    private var functionCallResult : T? = null

    override fun callFunction() {
        try{
            functionCallResult = sideEffect.invoke()
        }catch(e: Exception){
            logger.error("Failed to invoke side effect on object $receiver associated scenario message event ${this.toString()}, $e")
        }
    }

    override fun result() = functionCallResult

    fun parameter(name:String) : Any?{
        for(i in 0 until type.parameters.size-1 ){
            if (type.parameters[i+1].name == name)
                return parameters[i]
        }
        return null
    }

}


fun ObjectEvent<*,*>.parameterValuesEqual(otherObjectEvent : ObjectEvent<*,*>) : Boolean{
    if (parameters.size != otherObjectEvent.parameters.size)
        assert(false){"There must not be different numbers of parameters"}
    for (i in parameters.indices)
        if (parameters[i] != otherObjectEvent.parameters[i]) return false
    return true
}

fun ObjectEvent<*,*>.signatureEquals(receiver: Any, messageType : Any) : Boolean {
    if (!this.type.kFunctionEquals(messageType))
        return false
    if (receiver != this.receiver)
        return false
    return true
}

infix fun <R:Any, T> Any.sends(objectEvent : ObjectEvent<R, T>) = ObjectEvent(objectEvent.receiver, objectEvent.type, parameters = objectEvent.parameters, sender = this, sideEffect = objectEvent.sideEffect)

