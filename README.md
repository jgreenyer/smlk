# Welcome to SMLK


## What is SMLK?

SMLK is the **Scenario Modeling Language for Kotlin**.

SMLK is a rich, Kotlin-based incarnation of the [**behavioral programming**](http://www.wisdom.weizmann.ac.il/~bprogram/more.html) paradigm, which we also call **scenario-based programming**.


In scenario-based programming, developers are able to compose software from a set of independent **scenarios** that execute their behavior in parallel and synchronize on shared **events**.

This style of programming has several advantages:
1. **Scenarios encapsulate individual behavioral concerns**: it allows developers to write software in a way that is closely aligned with how people usually express requirements, namely by describing sequences of steps of how the software may, must, or must not behave reaction to certain events. When requirements change, the software can often evolve by only changing the scenarios corresponding to the changed requirements.
2. **Convenient programming of coordinated multi-threaded behavior**: Whereever a software performs many tasks at once, interacts with multiple external components/services/users, or implements a business logic with parallel flows and edge case handling, multi-threading is often the solution. Coordinating multiple threads, however, is difficult. Scenarios are similar to threads, but, in addition, scenario-based programming offers convenient mechanisms for spawning, progressing, synchronizing, and blocking scenarios.
 
## Why Kotlin?

Kotlin is a JVM-based language that has many features which make it the ideal language to realize a convenient and concise scenrio-based programming language: SMLK makes heavy use of Kotlin's [coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html) and language features such as higher-order functions, infix functions, extension functions, and use of lambdas outside of method parentheses.

## License

SMLK is released under the [MIT License](https://opensource.org/licenses/MIT).
